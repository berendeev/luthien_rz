import multiprocessing
import numpy as np
import os
import matplotlib
#matplotlib.use('Qt4Agg')
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.colors import LinearSegmentedColormap
import matplotlib.colors as col
import matplotlib.ticker as ticker
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib import gridspec
from matplotlib import rc
import pprint
import struct
import collections
import re

FieldsTitles=['Ez','Er','Ep','Bz','Br','Bp']

def readZondLine(WorkDir,fname):
	FieldsTitles=['Ez','Er','Ep','Bz','Br','Bp']
	#открыли файл
	FieldsName=WorkDir+'Fields/'+fname
	file = open(FieldsName, 'rb')
	#прочли параметры файла
	buf = file.read(4)
	
	Prop = struct.unpack("f", buf[:4])

	max_nodes = 1280#int(Prop[0])
	
	Data = np.fromfile(FieldsName, dtype="f4")
	max_timestep = (len(Data)-1) // (6*max_nodes)
	
	FieldData = collections.OrderedDict()
	
	for f in range(6):
		FieldData[FieldsTitles[f]] = np.zeros( (max_timestep,max_nodes) )

	for timestep in range( max_timestep ):
		for node in range(max_nodes):
			for f in range(6):
				FieldData[ FieldsTitles[f] ][timestep][node] = Data[timestep*6 *max_nodes + 6 * node + f + 1]
	return FieldData

def Plot1Ddens(DensData,Sort,time,ax,fig):
	print(sort,DensData[Sort].shape)
	#Data2D=np.swapaxes(DensData[Sort],0,1)
	Data2D=DensData[Sort]
	print(Data2D.shape)

#	Data2D=DensData[Sort]
	Data1D=Data2D[time]


#        Dens[PartSort]=data.reshape(( Nr, Nz))

	#	ax.set_xlim(0, int(SystemParameters['Ny']))
	xlist = np.arange(0,len(Data1D))
	ylist = [Data1D[x] for x in xlist]
#       print(ylist)
	ax.plot(xlist,ylist, color="g",linestyle=':',lw=2.4, zorder=4)
	ax.set_title(Sort)	
	ax.tick_params(axis='y', colors="g")
	ax.set_xlabel('$z$')
	#ax.set_ylabel('dens, r = '+str(Coord*0.025), color="g")
	ax.yaxis.grid() # horizontal lines

	return 0


WorkDir='../'
Fieldfiles=os.listdir(WorkDir+'Fields/')#получили список файлов в каталоге с полями (они есть всегда)
pattern = "ZondLine"
ZondLineFiles = []
for f in Fieldfiles:
	if re.match(pattern,f):
		ZondLineFiles.append(f)

for f in ZondLineFiles:
	fig = plt.figure(figsize=(16,6) )
	gs =gridspec.GridSpec(2,3)

	ZondLine = readZondLine(WorkDir,f)
	gs_x = gs_y = 0
	for sort in FieldsTitles:
		time = 4000
		Plot1Ddens(ZondLine,sort,time,fig.add_subplot(gs[gs_y,gs_x], label="1"),fig)
		gs_x += 1
		if gs_x == 3:
			gs_x = 0
			gs_y = 1
	plt.tight_layout()
	plt.savefig('../Anime/'+f+'.png', format='png', dpi=150)
	plt.close(fig)