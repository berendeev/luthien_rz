#include "SolverFDTD_PML.h"
#include "World.h"
#include "Mesh.h"

inline double pow3(double a){
  return a*a*a;
}

const long pml1 = DampCellsZ_glob[0] + CELLS_SHIFT + 1;
const long pml2 = DampCellsZ_glob[0] + CELLS_SHIFT + 1;
//const long pml = DampCellsZ_glob[0] + 1;

inline double shr(long i, long size_r){
  if(i<= size_r - pml1) return 0.;
  else return double(pml1-(size_r-i))/pml1;
}
inline double shz(long i){
  if(i>=pml1) return 0.; 
  else 
    return double(pml1-i)/pml1;
}
inline double shz(long i, long size_z){
  if((i<= size_z - pml2)) return 0.; 
  else 
    return double(pml2-(size_z-i))/pml2;
}

inline bool in_PML_z_left(long i, const Region& region){
	return i <= pml1 && region.boundType_d1[0] == BOUND_TYPE_OPEN;
}
inline bool in_PML_z_right(long i,const Region& region){
	return i <= pml1 && region.boundType_d1[1] == BOUND_TYPE_OPEN;
}
inline bool in_PML_r(long i){
	return i <= pml1;
}
bool in_PML(long i, long size_z, long j, long size_r,  const Region& region){
	return in_PML_z_left(i,region) || in_PML_z_right(size_z-i,region) || in_PML_r(size_r-j);
}
double sgm_z(long i, long size_z, double smzmax, const Region& region){
	if( in_PML_z_left(i,region) )
		return pow3(shz(i))*smzmax;
	if(in_PML_z_right(size_z-i,region) )
		return pow3(shz(i,size_z))*smzmax;
	return 0;	
}
double sgm_r(long i, long size_r, double smrmax, const Region& region){
	if( in_PML_r(size_r-i) )
		return pow3(shr(i,size_r))*smrmax;
	return 0;	
}

static void update_fieldsB(const Array2D<double3>& fieldE, Array2D<double3>& fieldB, Array2D<double3>& fieldBp, const Region& region){
    long i, j;
    const double dtp = 0.5 * Dt;
    const double rdr = 1. / Dr;
    const double rdz = 1. / Dz;
    const long size_z = fieldB.size_d1();
    const long size_r = fieldB.size_d2();
    const double smrmax = -2. * log(1.e-6) / (pml1 * Dr);
    const double smzmax = -2. * log(1.e-6) / (pml1 * Dz);
    double smz, smr;
	
    for(i = 0; i < size_z-1; i++){
		smz = sgm_z(i,size_z,smzmax,region);
		fieldB(i,0).r() = 0.;
		fieldBp(i,0).r() += dtp * ( (fieldE(i,1).z() - fieldE(i,0).z() ) * rdr );
		fieldBp(i,0).z() = (1. - dtp*smz) * fieldBp(i,0).z() + dtp * ( -(fieldE(i+1,0).r() - fieldE(i,0).r() ) * rdz);
		
		fieldB(i,0).p() = fieldBp(i,0).r() + fieldBp(i,0).z();	
    }
	
    fieldB(size_z-1,0).z() = fieldB(size_z-1,0).z() - dtp*(2*fieldE(size_z-1,1).p() )*rdr;
			

    for(i=0; i < size_z-1; i++){
	for(j=1; j<size_r-1; j++){

	    if(! in_PML(i,size_z,j,size_r,region) ){
			fieldB(i,j).z() += (- dtp*((j+1)*fieldE(i,j+1).p()-j*fieldE(i,j).p() )*rdr/(j+0.5));
			fieldB(i,j).r() += dtp*(fieldE(i+1,j).p()-fieldE(i,j).p() )*rdz;
			fieldB(i,j).p() += dtp*((fieldE(i,j+1).z() -fieldE(i,j).z() )*rdr-(fieldE(i+1,j).r()-fieldE(i,j).r() )*rdz);
	    }
	    else{
		smz = sgm_z(i,size_z,smzmax,region);

		smr = sgm_r(j,size_r,smrmax,region);
		fieldB(i,j).z() = (1.0-dtp*smr)*fieldB(i,j).z() - dtp*((j+1)*fieldE(i,j+1).p()-j*fieldE(i,j).p() )*rdr/(j+0.5);
		fieldB(i,j).r() = (1.0-dtp*smz)*fieldB(i,j).r() + dtp*(fieldE(i+1,j).p() - fieldE(i,j).p() )*rdz;		
		
		fieldBp(i,j).r() = (1.0-dtp*smr)*fieldBp(i,j).r() + dtp*((fieldE(i,j+1).z()-fieldE(i,j).z() )*rdr);
		fieldBp(i,j).z() = (1.0-dtp*smz)*fieldBp(i,j).z() + dtp*(-(fieldE(i+1,j).r()-fieldE(i,j).r() )*rdz);
		fieldB(i,j).p() = fieldBp(i,j).r() +fieldBp(i,j).z();
	    }
	}
    }

    i=size_z-1;
    for(j=1; j<size_r-1; j++){
	    
	if(!in_PML(i,size_z,j,size_r,region))
	    fieldB(i,j).z() += (- dtp*((j+1)*fieldE(i,j+1).p()-j*fieldE(i,j).p())*rdr/(j+0.5));
	else{
		smr = sgm_r(j,size_r,smrmax,region);
	    fieldB(i,j).z() = (1.0-dtp*smr)*fieldB(i,j).z() - dtp*((j+1)*fieldE(i,j+1).p()-j*fieldE(i,j).p())*rdr/(j+0.5);
	}
    }

    for(i=0; i<size_z-1; i++){
		j=size_r-1;
	
	if(!in_PML(i,size_z,j,size_r,region))
	      fieldB(i,j).r() += dtp*(fieldE(i+1,j).p()-fieldE(i,j).p() )*rdz;
	else{
		smz = sgm_z(i,size_z,smzmax,region);
	      fieldB(i,j).r() = (1.0-dtp*smz)*fieldB(i,j).r() + dtp*(fieldE(i+1,j).p()-fieldE(i,j).p())*rdz;
	}
    }
}


void solver_FDTD_PML(Array2D<double3>& fieldE, Array2D<double3>& fieldB,Array2D<double3>& fieldEp, Array2D<double3>& fieldBp, const Array2D<double3>& fieldJ, const World& world){
    long i, j;
    const double rdr = 1. / Dr;
    const double rdz = 1. / Dz;
    const long size_z = fieldE.size_d1();
    const long size_r = fieldE.size_d2();
    const double smrmax = -2. * log(1.e-6) / (pml1 * Dr);
    const double smzmax = -2. * log(1.e-6) / (pml1 * Dz);
    static Array2D<double> Ep0(2,size_r);
    static Array2D<double> Er0(2,size_r);
    double smz, smr;

    for (j = 0; j < size_r; ++j){
		Ep0(0,j) = fieldE(1,j).p();
		Ep0(1,j) = fieldE(size_z-2,j).p();	
		Er0(0,j) = fieldE(1,j).r();
		Er0(1,j) = fieldE(size_z-2,j).r();      
    }

    update_fieldsB(fieldE, fieldB, fieldBp, world.region);
	
    for(i = 1; i < size_z - 1; ++i){
      
	if(!in_PML(i,size_z,j,size_r,world.region)){
	    fieldE(i,0).r() += (- Dt*rdz*(fieldB(i,0).p()-fieldB(i-1,0).p()) - Dt*fieldJ(i,0).r());
	    fieldE(i,0).z() += (4.0*Dt*rdr*(fieldB(i,0).p()) - Dt*fieldJ(i,0).z());
	    fieldE(i,0).p() = 0.0;
	}
	else{
		smz = sgm_z(i,size_z,smzmax,world.region);

	    fieldE(i,0).r() = (1.0-Dt*smz)*fieldE(i,0).r() - Dt*rdz*(fieldB(i,0).p()-fieldB(i-1,0).p()) - Dt*fieldJ(i,0).r();
	    fieldE(i,0).z() += (4.0*Dt*rdr*(fieldB(i,0).p()) - Dt*fieldJ(i,0).z());
	    fieldE(i,0).p() = 0.0;
	    fieldEp(i,0).r() = 0.0;
	    fieldEp(i,0).z() = 0.0;	  
	}
	
    }
	
    i = 0;
    for(j = 1; j < size_r; ++j){
	    
	if(!in_PML(i,size_z,j,size_r,world.region))
	    fieldE(i,j).z() += ( Dt*rdr*((j+0.5)*fieldB(i,j).p()-(j-0.5)*fieldB(i,j-1).p())/j - Dt*fieldJ(i,j).z());
	else{
		smr = sgm_r(j,size_r,smrmax,world.region);
	    fieldE(i,j).z() = (1.0-Dt*smr)*fieldE(i,j).z() + Dt*rdr*((j+0.5)*fieldB(i,j).p()-(j-0.5)*fieldB(i,j-1).p())/j - Dt*fieldJ(i,j).z();
	}
    }
	
    for(i = 1; i < size_z-1; ++i){
	for(j = 1; j < size_r; ++j){
		
		if(!in_PML(i,size_z,j,size_r,world.region)){
		    fieldE(i,j).z() += ( Dt*rdr*((j+0.5)*fieldB(i,j).p()-(j-0.5)*fieldB(i,j-1).p())/j - Dt*fieldJ(i,j).z());
		    fieldE(i,j).r() += (- Dt*rdz*(fieldB(i,j).p()-fieldB(i-1,j).p()) - Dt*fieldJ(i,j).r());
		    fieldE(i,j).p() += Dt*((fieldB(i,j).r()-fieldB(i-1,j).r())*rdz- (fieldB(i,j).z()-fieldB(i,j-1).z())*rdr-fieldJ(i,j).p());
		} else{
			smz = sgm_z(i,size_z,smzmax,world.region);

			smr = sgm_r(j,size_r,smrmax,world.region);
		    fieldE(i,j).z() = (1.0-Dt*smr)*fieldE(i,j).z() + Dt*rdr*((j+0.5)*fieldB(i,j).p()-(j-0.5)*fieldB(i,j-1).p())/j - Dt*fieldJ(i,j).z();
		    fieldE(i,j).r() = (1.0-Dt*smz)*fieldE(i,j).r() - Dt*rdz*(fieldB(i,j).p()-fieldB(i-1,j).p()) - Dt*fieldJ(i,j).r(); 
		    
		    fieldEp(i,j).r() = (1.0-Dt*smr)*fieldEp(i,j).r() - Dt*rdr*(fieldB(i,j).z()-fieldB(i,j-1).z()) -0.5*Dt*fieldJ(i,j).p();
		    fieldEp(i,j).z() = (1.0-Dt*smz)*fieldEp(i,j).z() + Dt*rdz*(fieldB(i,j).r()-fieldB(i-1,j).r())-0.5*Dt*fieldJ(i,j).p();
		    fieldE(i,j).p() = fieldEp(i,j).r()+ fieldEp(i,j).z();
		}
	}
    }
    
    exchange_fieldsE(fieldE,world.MPIconf);
    exchange_fieldsE(fieldEp,world.MPIconf);
	
    if(world.region.boundType_d1[0] == BOUND_TYPE_OPEN){
	for(j = 0; j < size_r; ++j){
	    double Kabc = (Dt - Dz) / (Dt + Dz);
	    fieldE(0,j).p() = Ep0(0,j)+Kabc*(fieldE(1,j).p()-fieldE(0,j).p());
	    fieldE(0,j).r() = Er0(0,j)+Kabc*(fieldE(1,j).r()-fieldE(0,j).r());
	  }
    }
    if(world.region.boundType_d1[1] == BOUND_TYPE_OPEN){
	for(j = 0; j < size_r; ++j){
	    double Kabc = (Dt - Dz) / (Dt + Dz);
	    fieldE(size_z-1,j).p() = Ep0(1,j)+Kabc*(fieldE(size_z-2,j).p()-fieldE(size_z-1,j).p());
	    fieldE(size_z-1,j).r() = Er0(1,j)+Kabc*(fieldE(size_z-2,j).r()-fieldE(size_z-1,j).r());
	  }

    }

   update_fieldsB(fieldE, fieldB, fieldBp, world.region);
			
} 
