#include "World.h"
#include "Mesh.h"
#include "SolverFDTD.h"

static void update_fieldsB(const Array2D<double3>& fieldE, Array2D<double3>& fieldB){
    long i, j;
    const double dtp = 0.5 * Dt;
    const double rdr = 1. / Dr;
    const double rdz = 1. / Dz;
    const long size_z = fieldB.size_d1();
    const long size_r = fieldB.size_d2();
	
    for(i = 0; i < size_z-1; i++){
      
	fieldB(i,0).r() = 0.0;
	fieldB(i,0).z() += (- dtp*(2*fieldE(i,1).p() )*rdr);
	fieldB(i,0).p() += dtp*((fieldE(i,1).z() - fieldE(i,0).z() ) * rdr - (fieldE(i+1,0).r() - fieldE(i,0).r() )*rdz);	
    }
	
    fieldB(size_z-1,0).z() += (- dtp*(2*fieldE(size_z-1,1).p() )*rdr);
			

    for(i=0; i < size_z-1; i++){
	for(j=1; j<size_r-1; j++){
		fieldB(i,j).z() += (- dtp*((j+1)*fieldE(i,j+1).p()-j*fieldE(i,j).p() )*rdr/(j+0.5));
		fieldB(i,j).r() += dtp*(fieldE(i+1,j).p() - fieldE(i,j).p() )*rdz;
		fieldB(i,j).p() += dtp*((fieldE(i,j+1).z() - fieldE(i,j).z() )*rdr-(fieldE(i+1,j).r() - fieldE(i,j).r() )*rdz);
	}
    }

    i = size_z-1;
    for(j=1; j<size_r-1; j++){
	    fieldB(i,j).z() += (- dtp*((j+1)*fieldE(i,j+1).p()-j*fieldE(i,j).p())*rdr/(j+0.5));
    }

    for(i = 0; i < size_z - 1; i++){
	j = size_r - 1;
	fieldB(i,j).r() += dtp * (fieldE(i+1,j).p() - fieldE(i,j).p() ) * rdz;
    }
}


void solver_FDTD(Array2D<double3>& fieldE, Array2D<double3>& fieldB, const Array2D<double3>& fieldJ, const World& world){
    long i, j;
    const double rdr = 1. / Dr;
    const double rdz = 1. / Dz;
    const long size_z = fieldE.size_d1();
    const long size_r = fieldE.size_d2();
    static Array2D<double> Ep0(2,size_r);
    static Array2D<double> Er0(2,size_r);

    for (j = 0; j < size_r; ++j){
	Ep0(0,j) = fieldE(1,j).z();
	Ep0(1,j) = fieldE(size_z-2,j).p();	
	Er0(0,j) = fieldE(1,j).r();
	Er0(1,j) = fieldE(size_z-2,j).r();      
    }

    update_fieldsB(fieldE, fieldB);
	
    for(i = 1; i < size_z - 1; ++i){
	fieldE(i,0).r() += (- Dt*rdz*(fieldB(i,0).p() - fieldB(i-1,0).p() ) - Dt*fieldJ(i,0).r() );
	fieldE(i,0).z() += (4.0*Dt*rdr*(fieldB(i,0).p() ) - Dt*fieldJ(i,0).z() );
	fieldE(i,0).p() = 0.0;
    }
	
    i = 0;
    for(j = 1; j < size_r; ++j){
	    fieldE(i,j).z() += ( Dt*rdr*((j+0.5)*fieldB(i,j).p()-(j-0.5)*fieldB(i,j-1).p())/j - Dt*fieldJ(i,j).z() );
    }
	
    for(i = 1; i < size_z-1; ++i){
	for(j = 1; j < size_r; ++j){
		    fieldE(i,j).z() += ( Dt*rdr*((j+0.5)*fieldB(i,j).p() - (j-0.5)*fieldB(i,j-1).p() ) / j - Dt*fieldJ(i,j).z() );
		    fieldE(i,j).r() += (- Dt*rdz*(fieldB(i,j).p()-fieldB(i-1,j).p() ) - Dt*fieldJ(i,j).r() );
		    fieldE(i,j).p() += Dt*((fieldB(i,j).r()-fieldB(i-1,j).r() ) * rdz 
		    					- (fieldB(i,j).z() - fieldB(i,j-1).z() ) * rdr - fieldJ(i,j).p() );
	}
    }
    
    exchange_fieldsE(fieldE,world.MPIconf);
	
    if(world.MPIconf.is_first_line()){
	for(j = 0; j < size_r; ++j){
	    double Kabc = (Dt - Dz) / (Dt + Dz);
	    fieldE(0,j).p() = Ep0(0,j) + Kabc*(fieldE(1,j).p() - fieldE(0,j).p() );
	    fieldE(0,j).r() = Er0(0,j) + Kabc*(fieldE(1,j).r() - fieldE(0,j).r() );
	  }
    }
    
    if(world.MPIconf.is_last_line() ){
	  for(j = 0; j < size_r; ++j){
	    double Kabc = (Dt - Dz) / (Dt + Dz);
	    fieldE(size_z-1,j).p() = Ep0(1,j) + Kabc*(fieldE(size_z-2,j).p() - fieldE(size_z-1,j).p() );
	    fieldE(size_z-1,j).r() = Er0(1,j) + Kabc*(fieldE(size_z-2,j).r() - fieldE(size_z-1,j).r() );
	  }

    }

   update_fieldsB(fieldE, fieldB);
			
} 

