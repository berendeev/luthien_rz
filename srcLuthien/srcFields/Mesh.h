#ifndef MESH_H_
#define MESH_H_
#include "World.h"
#include "Laser.h"
#include <mpi.h>
#include "Coil.h"
#include <map>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <sys/types.h>
#include <sys/stat.h>
#include <assert.h>

double calc_energy_field(const Array2D<double3>& field);
void exchange_fieldsE(Array2D<double3>& field,const MPI_Topology &MPIconf);

struct Mesh{
    Mesh(const World& world);

    Array2D<double3> fieldE;
    Array2D<double3> fieldB;
    Array2D<double3> fieldEp;
    Array2D<double3> fieldBp;
    Array2D<double3> fieldJ;
    Array2D<double3> fieldB0;
    std::vector<Laser> lasers;
    int solverType = EM_SOLVER;
    
    void set_fields(const World& world);

    void set_uniform_fields();
    void read_from_recovery(const MPI_Topology &MPIconf);
    void write_recovery(const MPI_Topology &MPIconf) const;
    void update(const World& world,long timestep);
    double3 get_fieldE_in_pos(const double2& POS) const;
    double3 get_fieldE_in_cell(long i, long j)  const;
    double3 get_fieldB_in_cell(long i, long j)  const;
    double get_fieldE_energy() const{
        return calc_energy_field(fieldE);
    };
    double get_fieldB_energy() const{
        return calc_energy_field(fieldB);
    };
    double get_fieldB0_energy() const{
        return calc_energy_field(fieldB0);
    };
    void reduce_current(const MPI_Topology &MPIconf);
    ~Mesh(){
    }

};


#endif 
