#include "Coil.h"

double CoilsArray::get_integ_r(double z,double r,double R){
    double sum = 0;
    double znam;
    for (auto i = 0; i < N; i++){
        znam =  R*R + z*z + r*r - 2*R*r*cs[i];
        sum += hp*(cs[i] / (znam*sqrt(znam) ) );
    }
    return sum;
}
double CoilsArray::get_integ_z(double z,double r,double R){
    double sum = 0;
    double znam;

    for (auto i = 0; i < N; i++){
        znam =  R*R + z*z + r*r - 2*R*r*cs[i];
        sum += hp*((R- r*cs[i] )/ (znam*sqrt(znam) ) );
    }
    return sum;
}


double3 CoilsArray::get_B(double z,double r){
    double Br=0,Bz=0;
    for (const auto& coil : coils){
    	z = z - coil.z0;
    	Br += coil.I*coil.R*z*get_integ_r(z,r,coil.R);
    	Bz += coil.I*coil.R*get_integ_z(z,r,coil.R);
	}
    return double3(Bz,Br,0.);
}


void set_coils( Array2D<double3>& fieldB,const World& world){
  auto size_z = fieldB.size_d1();
  auto size_r = fieldB.size_d2();
  double rr,zz;
  double3 B;
  CoilsArray coils;

        //std::cout<< coil.z0 << " " <<coil.R << " "<<amp  <<"\n";
    for( auto i = 0; i < size_z; ++i){
      for( auto j = 0; j < size_r; ++j){
      	    zz = i*Dz+world.region.origin;
      	    rr = j*Dr; 
        	B = coils.get_B(zz,rr+0.5*Dr);
        	fieldB(i,j).z() += B.z();
            B = coils.get_B(zz+0.5*Dz,rr);
        	fieldB(i,j).r() += B.r();
        //if (fieldB(i,j).z() > 1e-10)
      }
    }
    std::cout<<"Coils configuration set successfull!\n";
}
