#include "Mesh.h"
#include "World.h"
#include "Shape.h"
#include "SolverFDTD.h"
#include "SolverFDTD_PML.h"
#include "Damping.h"

Mesh::Mesh(const World& world) : 
          fieldE(world.region.nn),fieldB(world.region.nn),
          fieldEp(world.region.nn),fieldBp(world.region.nn),fieldJ(world.region.nn),fieldB0(world.region.nn){
    
    if (RECOVERY){ 
      read_from_recovery(world.MPIconf);
    } 
    else{
      set_fields(world);
    }

    std::vector< std::vector<std::string> > stringParams;
    read_params_to_string("Lasers","./LasParams.cfg",stringParams);
    for( const auto &params  : stringParams){
        lasers.emplace_back(params);
    }
}

void Mesh::set_fields(const World& world){
	set_uniform_fields();
	if (BCoil[0] > 0){
		set_coils(fieldB,world);
	}
	fieldB0 = fieldB;
	//fieldB = double3(0.,0.,0.);
} 

void Mesh::set_uniform_fields(){
  auto size_z = fieldE.size_d1();
  auto size_r = fieldE.size_d2();
  for( auto i = 0; i < size_z; ++i){
    for( auto j = 0; j < size_r; ++j){
      fieldE(i,j) = 0;
      fieldB(i,j) = double3(BUniform[0], BUniform[1], BUniform[2] );
    }
  }
}

double calc_energy_field(const Array2D<double3>& field){
  double potE = 0;
  long i_max = field.size_d1() - (2 * SHAPE_SIZE - 1);
  long j_max = field.size_d2() - ( 2 * SHAPE_SIZE - 1);
  for(auto i = 0; i < i_max; ++i){
    for(auto j = 0; j < j_max; ++j){
      potE += dot(field(i,j),field(i,j) ) * (2*PI*(j+0.5)*Dr*Dr*Dz);      
    }
  }
  potE*=0.5;
  return potE;
}


void Mesh::reduce_current(const MPI_Topology &MPIconf){

  int left, right;
  int tag = 1;
  MPI_Status status;
  int bufSize = (2 * SHAPE_SIZE - 1)*fieldJ.size_d2();
  long sendIndx; 
  static double3 *recvBuf = new double3[bufSize];
  
  MPI_Allreduce ( MPI_IN_PLACE, &fieldJ.data(0), 3*fieldJ.size_d1()*fieldJ.size_d2(), MPI_DOUBLE, MPI_SUM, MPIconf.comm_depth() ) ;
  
  MPI_Barrier(MPI_COMM_WORLD);

  
  if (MPIconf.rank_line() < MPIconf.size_line() - 1)
    right = MPIconf.rank_line() + 1;
  else
    right = 0;
  
  if (MPIconf.rank_line() > 0)
    left = MPIconf.rank_line() - 1;
  else
    left = MPIconf.size_line() - 1;


  sendIndx = fieldJ.size_d2()*fieldJ.size_d1()-bufSize;
  // fromRight to Left
  MPI_Sendrecv(&fieldJ.data(sendIndx), 3*bufSize, MPI_DOUBLE_PRECISION, right, tag, 
                           recvBuf, 3*bufSize, MPI_DOUBLE_PRECISION, left, tag, 
                           MPIconf.comm_line(), &status);
  
  for (long i = 0; i< bufSize; i++){
    fieldJ.data(i) += recvBuf[i];

  }
  
  MPI_Sendrecv(&fieldJ.data(0), 3*bufSize, MPI_DOUBLE_PRECISION,left , tag, 
                           &fieldJ.data(sendIndx), 3*bufSize, MPI_DOUBLE_PRECISION, right, tag, 
                           MPIconf.comm_line(), &status);

  
}

void exchange_fieldsE(Array2D<double3>& fieldE, const MPI_Topology &MPIconf){

  int tag = 1;
  MPI_Status status;
  const long size_z = fieldE.size_d1();
  const long size_r = fieldE.size_d2();
  const long bufSize = size_r; 
  long sendIndx, recvIndx; 
  static double3 *recvBuf = new double3[bufSize];
  long shift = 2 * SHAPE_SIZE - 1;
  
  auto right = MPIconf.next_line();
  auto left = MPIconf.prev_line();

  sendIndx = (size_z-shift)*size_r;
  // fromRight to Left
  
  MPI_Sendrecv(&fieldE.data(sendIndx), 3*bufSize, MPI_DOUBLE_PRECISION, right, tag, 
                           recvBuf, 3*bufSize, MPI_DOUBLE_PRECISION, left, tag, 
                           MPIconf.comm_line(), &status);
  if( !MPIconf.is_first_line() ){
      for (auto i = 0; i< bufSize; i++){
        fieldE.data(i) = recvBuf[i];
      }
  }
  
  sendIndx = (shift-1)*size_r;
  MPI_Sendrecv(&fieldE.data(sendIndx), 3*bufSize, MPI_DOUBLE_PRECISION,left , tag, 
                           recvBuf, 3*bufSize, MPI_DOUBLE_PRECISION, right, tag, 
                           MPIconf.comm_line(), &status);
  
  recvIndx = (size_z-1)*size_r;  
     
  if( !MPIconf.is_last_line() ){

      for (auto i = 0; i< bufSize; i++){
      fieldE.data(recvIndx+i) = recvBuf[i];
      }
  }
  
  
  MPI_Barrier(MPIconf.comm_line() );
  
}

double3 Mesh::get_fieldE_in_cell(long i, long j)  const{
  double3 E;
  E.z() = 0.5 * (fieldE(i,j).z() + fieldE(i,j+1).z() );
  E.r() = 0.5 * (fieldE(i,j).r() + fieldE(i+1,j).r() );
  E.p() = 0.25 * (fieldE(i,j).p() + fieldE(i+1,j).p() + fieldE(i,j+1).p() + fieldE(i+1,j+1).p() );
  return E;
}
double3 Mesh::get_fieldB_in_cell(long i, long j)  const{
  double3 B;
  B.z() = 0.5 * (fieldB(i,j).z() + fieldB(i+1,j).z() );
  B.r() = 0.5 * (fieldB(i,j).r() + fieldB(i,j+1).r() );
  B.p() = fieldB(i,j).p();
  return B;
}
inline double Shape2(const double& dist){
  double d = fabs(dist);
  if ( d <= 0.5 ) return (0.75 - d * d);
  else 
      if (d < 1.5)  return ((d - 1.5) * (d-1.5) * 0.5);
      else return 0.;
}

double3 Mesh::get_fieldE_in_pos(const double2& POS)  const{
  constexpr auto SMAX = 4; //2*SHAPE_SIZE;
  alignas(64) double sz[SMAX];
  alignas(64) double sr[SMAX];
  alignas(64) double sdr[SMAX];
  alignas(64) double sdz[SMAX];
  alignas(64) double3 E;
  double zz, rr, arg;
  double snm1, snm2, snm4;
  long zk, rk, indz, indr;
  
  zz = POS.z() / Dz;
  rr = POS.r() / Dr;
      
  zk = long(zz);
  rk = long(rr);
  
  for(auto n = 0; n < SMAX; ++n){
    arg = -zz + double(zk - CELLS_SHIFT + n);
    sz[n] = Shape2(arg);
    sdz[n] = Shape2(arg + 0.5);
    arg = -rr + double(rk - CELLS_SHIFT + n);
    sr[n] = Shape2(arg);
    sdr[n] = Shape2(arg + 0.5);
  }
    
  for( auto m = 0; m < SMAX; ++m){
    for( auto n = 0; n < SMAX; ++n){      

      
      snm1 = sdr[n] * sz[m];
      snm4 = sr[n] * sz[m];
      snm2 = sr[n] * sdz[m];
      indz = zk + m;
      indr = rk - CELLS_SHIFT + n;
      
      if(indr >= 0){      
        E += double3(snm2,snm1,snm4) * fieldE(indz,indr);
      } 
      else {
        E.z() += snm2 * fieldE(indz,-indr).z();
        E.r() -= snm1 * fieldE(indz,-indr-1).r();
        E.p() += snm4 * fieldE(indz,-indr).p();
      }
    }
  }
  
    return E;
}

void Mesh::update(const World& world,long timestep){
  	if (!UPD_FIELDS) return;
    
    fieldB -= fieldB0;

    switch(solverType){

    	case EM_SOLVER_FDTD:

      		if (EM_BOUND == EM_BOUND_PML){
      			solver_FDTD_PML(fieldE, fieldB,fieldEp, fieldBp, fieldJ, world);
      		} else {
      			solver_FDTD(fieldE, fieldB, fieldJ, world);
      		}
      		break;
    	//case EM_SOLVER_NONE:
    	//	break;
        default:
            break;
  	}
  	if (EM_BOUND == EM_BOUND_DAMP){
  		damping_fields(fieldE, fieldB,world.region);
	}
	
    fieldB += fieldB0;

}