#ifndef SolverFDTD_H_
#define SolverFDTD_H_

void solver_FDTD(Array2D<double3>& fieldE, Array2D<double3>& fieldB, const Array2D<double3>& fieldJ, const World& world);

#endif 	
