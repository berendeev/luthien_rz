#include "World.h"
#include "Damping.h"
void damping_func(double3& source, long i, long maxi){
    double koeff=0.8;
    double a, damp;

    a = (1.0-koeff)/(maxi*maxi);
    damp = a * i * i + koeff;
    source = source * damp;	
} 

void damping_fields(Array2D<double3>& fieldE, Array2D<double3>& fieldB,const Region& domain){
    long i,j,i1,j1;
    double dampCells;
    const long cellsRP = 1.1 *PlasmaCellsR_glob;
    long dampCellsZ[2];
    long max_indz = domain.nd1-1;
    long max_indr = domain.nd2-1;
    dampCellsZ[0] = domain.dampCells_d1[0] + CELLS_SHIFT + 1;
    dampCellsZ[1] = domain.dampCells_d1[1] + CELLS_SHIFT + 1;
  
    if (domain.boundType_d1[0] == BOUND_TYPE_OPEN) { 
      	for(i = 0; i < dampCellsZ[0]; i++){
	  		for (j = 0; j <= max_indr; j++){

				if( ( i >= dampCellsZ[0] - domain.dampCellsForce_d1[0]) && j <cellsRP ){
				    i1 = i - (dampCellsZ[0] - domain.dampCellsForce_d1[0]);
				    dampCells = domain.dampCellsForce_d1[0];
				}
				else{ 
				    i1 = i;
				    dampCells = dampCellsZ[0];
				}
		
				damping_func(fieldE(i,j), i1, dampCells);											
				damping_func(fieldB(i,j), i1, dampCells);
			}
		}
    }

    if (domain.boundType_d1[1] == BOUND_TYPE_OPEN) {
      	for(i = max_indz; i > max_indz - dampCellsZ[1]; i--){
	  	    for (j = 0; j <= max_indr; j++){

			    if((i <= max_indz - (dampCellsZ[1] - domain.dampCellsForce_d1[1])) && j <cellsRP){
				    i1 = -i + (max_indz - (dampCellsZ[1] - domain.dampCellsForce_d1[1]));
				    dampCells = domain.dampCellsForce_d1[1];
			    } else{ 
				    i1 = -(i - max_indz);
				    dampCells = dampCellsZ[1];
			    }
	    		
				damping_func(fieldE(i,j), i1, dampCells);					
				damping_func(fieldB(i,j), i1, dampCells);
	    	}
		}
    }

    if (domain.boundType_d2 == BOUND_TYPE_OPEN) {

	    for( i = 0; i <= max_indz; ++i){			
			for( j = max_indr; j > max_indr - domain.dampCells_d2; --j){
							
			    j1 = - j + max_indr;
			    dampCells = domain.dampCells_d2;

			    damping_func(fieldE(i,j), j1, dampCells);
								
			    damping_func(fieldB(i,j), j1, dampCells);
			}	
	    }
	}
  
}


