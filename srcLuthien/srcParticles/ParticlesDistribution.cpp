#include "Particles.h"
#include "World.h"
#include "Shape.h"

inline double pow4(double a){
  return a*a*a*a;
}

inline double smooth_sin(double dist){

  if (fabs(dist) > 1.) {
  	return 0;
  }
  else{
  	return 0.5*(1.+cos(dist*PI));
  }
}

double set_density_neutron(double2 x){
	return exp( -pow4( (x(0))/(1.2*Dz*PlasmaCellsZ_glob/2) ));
//	return exp( -pow4( (x(0)-Dz*PlasmaCellsZ_glob/2)/(1.2*Dz*PlasmaCellsZ_glob/2) ));
}


void ParticlesArray::set_space_distribution(){
	
	long startZ = DampCellsZ_glob[0];
	long endZ = NumCellsZ_glob - DampCellsZ_glob[1];
	long endR = PlasmaCellsR_glob;

	if( initDist == "StrictUniform"){
		set_strictUniformRZ(startZ, endZ, endR, _world.region, _world.MPIconf);
	}
	if( initDist == "UnderForceLine_Z_R"){
		std::cout<<"Setting particles...\n";
		set_under_force_line(startZ, endZ, endR, distParams[0], distParams[1], _world.region, _world.MPIconf);
	}
	

	if( initDist == "UniformCosZ_dn_k"){
		set_strictCosRZ(startZ, endZ, endR, distParams[0], distParams[1], _world.region, _world.MPIconf);
	}
		#ifdef PARTICLE_MASS
			if (option.smoothMass == 1){
				set_smooth_mass();
			}
			else{
					for (auto k = 0; k < size(); ++k){
						particlesData(k).mass = _mass;
					}
			}
		#endif
		
	//	if (charge == 0) 
	//		set_distribution_density(world.region,set_density_neutron);
}
void  ParticlesArray::set_pulse_distribution(){

		auto sigma = temperature;
		for( auto k = 0; k < size(); ++k){
		  	double phi = 2. * PI * Uniform01();
			double pxy = Gauss(sigma);
			particlesData(k).PULS.z() = Gauss(sigma);
			particlesData(k).PULS.r() = pxy * sin(phi);
			particlesData(k).PULS.p() = pxy * cos(phi);
		} 
	
}




void ParticlesArray::set_strictUniformRZ(long startZ, long endZ, long endR, const Region& domain, const MPI_Topology& MPIconf){
    const double deltaZ = Dz / NumPartPerLine;
    const double deltaR = Dr / NumPartPerLine;
    Particle particle;
    double r, z;	

    z = -0.5 * deltaZ;

    for( auto j = 0; j < NumPartPerLine * (endZ - startZ); ++j){
	z += deltaZ;

	r = -0.5 * deltaR;
		if( domain.in_region(z + Dz * startZ) ){

		    for ( auto i = 0; i < NumPartPerLine * endR; ++i){
				r += deltaR;
					    particle.POS.z() = z + Dz * startZ - domain.origin;
					    particle.POS.r() = r;
					
						if( long( r / Dr + 0.5 ) > 0)
						    particle.mpw = density*(2.0*PI*(long(r/Dr+0.5))*Dr*Dr*Dz) / (NumPartPerLine*NumPartPerLine);
						else 
						    particle.mpw =  density*(PI*Dr*Dr*Dz*0.25) / (NumPartPerLine*NumPartPerLine);
						add_particle_scatter(particle);		
		    }
		}
    }
}


void ParticlesArray::set_strictCosRZ(long startZ, long endZ, long endR, double delta_n0, double k0, const Region& domain, const MPI_Topology& MPIconf){
    const long Nd = 10000000;
    const double hd = Dz * (endZ - startZ) / Nd;
    const double deltaR = Dr / NumPartPerLine;
    double r, z;	
    double SS = 0.;
	Particle particle;

    for (auto i = 0; i < Nd; ++i){
	SS += ( (1.0 + delta_n0 * cos( i * k0 * hd)) * hd);
    }

    auto NpZ = NumPartPerLine  * (endZ - startZ);
	
    auto mySS = SS / NpZ;
 

///    long numPart = 0;
    
    z = -0.5 * mySS;

    for( auto j = 0; j < NumPartPerLine * (endZ - startZ); ++j){
	z += (mySS / (1.0 + delta_n0 * cos( k0 * z) ) );
	r = -0.5 * deltaR;
	if( domain.in_region(z + Dz * startZ) ){

	    for ( auto i = 0; i < NumPartPerLine * endR; ++i){
		r += deltaR;
		    particle.POS.z() = z + Dz * startZ - domain.origin;
		    particle.POS.r() = r;
		
			if( long( r / Dr + 0.5 ) > 0)
			    particle.mpw = density*(2.0*PI*(long(r/Dr+0.5))*Dr*Dr*Dz) / (NumPartPerLine*NumPartPerLine);
			else 
			    particle.mpw =  density*(PI*Dr*Dr*Dz*0.25) / (NumPartPerLine*NumPartPerLine);

				add_particle_scatter(particle);

		
	  }
	
     
	}

    }
		
}

void ParticlesArray::set_under_force_line(long startZ, long endZ, long endR, double zf, double rf, const Region& domain, const MPI_Topology& MPIconf){
    const double deltaZ = Dz / NumPartPerLine;
    const double deltaR = Dr / NumPartPerLine;
    Particle particle;
    double r, z;	
    CoilsArray coils;
    double3 B;
    double ds,zn,rn,zc;
    bool is_convergence = true;
	std::ofstream fout;
    z = -0.5 * deltaZ;
    //std::cout << "Z = " << coils.coils[0].z0 << " R = " << coils.coils[0].R <<"\n";
        //fout.open( "fline.dat"+std::to_string(MPIconf.rank() ) ); 

    for( auto j = 0; j < NumPartPerLine * (endZ - startZ); ++j){
    	is_convergence = true;
	z += deltaZ;
	r = -0.5 * deltaR;
		if( domain.in_region(z + Dz * startZ) ){
			B = coils.get_B(zf,rf);
			ds = 2*Dz;//0.5*deltaZ;
			zn = zf + ds*B.z()/sqrt(B.z()*B.z()+B.r()*B.r()); 
			rn = rf + ds*B.r()/sqrt(B.z()*B.z()+B.r()*B.r());
			
			if(fabs(zn-(z+ Dz * startZ) ) >=fabs(zf-(z+ Dz * startZ) ) ) ds=-ds;
			while(is_convergence ){
				B = coils.get_B(zn,rn);
				zc = zn + ds*B.z()/sqrt(B.z()*B.z()+B.r()*B.r()); 
				rn = rn + ds*B.r()/sqrt(B.z()*B.z()+B.r()*B.r());
				if (fabs(zc-(z+ Dz * startZ) ) >= fabs(zn- (z+ Dz * startZ) ) ) is_convergence = false;
				//fout<< zc << " " << rn <<"\n"; 
				zn = zc; 

			}
		    for ( auto i = 0; i < NumPartPerLine * endR; ++i){
				r += deltaR;
					    particle.POS.z() = z + Dz * startZ - domain.origin;
					    particle.POS.r() = r;
					
						if( long( r / Dr + 0.5 ) > 0)
						    particle.mpw = density*(2.0*PI*(long(r/Dr+0.5))*Dr*Dr*Dz) / (NumPartPerLine*NumPartPerLine);
						else 
						    particle.mpw =  density*(PI*Dr*Dr*Dz*0.25) / (NumPartPerLine*NumPartPerLine);
						if(r<=rn)
							add_particle_scatter(particle);		
		    }
		}
    }
}



void ParticlesArray::set_smooth_mass(){
	#ifdef PARTICLE_MASS
	    double r, z;
	    double2 POS;

		for (auto k = 0; k < size(); ++k){
			z = particlesData(k).POS.z();
			r = particlesData(k).POS.r();
			POS =  _world.region.get_coord_glob(double2(z,r));  
			if (POS.z() < 0.5 * Dz * NumCellsZ_glob) 
				z = POS.z() - Dz * DampCellsZ_glob[0];
			else
				z = Dz * NumCellsZ_glob - Dz * DampCellsZ_glob[1] - POS.z();
			particlesData(k).mass = (option.smoothMassMax * smooth_sin( z  / option.smoothMassSize ) + 1. ) * _mass;
		}
	#else
		std::cout << "Class Particle has not a mass\n";
	#endif
}

void ParticlesArray::set_distribution_density(std::function<double(double2 )> set_density){

    double r, z, f;
    double2 POS;

	for (auto k = 0; k < size(); ++k){
		z = particlesData(k).POS.z() - Dz * DampCellsZ_glob[0];
		r = particlesData(k).POS.r();
		POS = _world.region.get_coord_glob(double2(z,r));  
		f = set_density(POS);
		particlesData(k).mpw *= f;
	}

		
}