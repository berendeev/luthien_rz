#include "Particles.h"

void ParticlesArray::source_uniform_from_bound_mpw(long timestep){
    double energyInj = 0;
	double vb = velocity;
	double sigma = temperature;
    bool LeaftInj =  (vb>0. && _world.region.boundType_d1[0] == BOUND_TYPE_OPEN);
	bool RightInj =  (vb<0. && _world.region.boundType_d1[1] == BOUND_TYPE_OPEN);

	if(!( LeaftInj || RightInj )) return;

	double pp,pz,pb,gama,pr;
    const double deltaR = Dr / NumPartPerLine;
    Particle particle;
    double r, z;	

    for( auto j = 0; j < NumPartPerCell * long(width/Dr); ++j){
		z = Dz*Uniform01();

		bool CondInj = z<= Dt*fabs(vb );

		if( !CondInj ) continue;

				r = width*Uniform01();
				double phi = 2. * PI * Uniform01();
				double pxy = Gauss(sigma);
				pr = pxy * sin(phi);
				pp = pxy * cos(phi);
				
				pb = vb / sqrt(1.0-vb*vb);
				
				pz = pb+Gauss(sigma); 
				particle.POS.r() = r ;
				particle.PULS.z() = pz;
				if(LeaftInj ){
					  particle.POS.z() = z + Dz * _world.region.dampCells_d1[0];
					  particle.PULS.p() = pp;
					  particle.PULS.r() = pr;
				}
				else if (RightInj ){
					  particle.POS.z() = Dz * _world.region.numCells_d1 - z - Dz *  _world.region.dampCells_d1[1];
					  particle.PULS.p() = - pp;
					  particle.PULS.r() = - pr;
				}
							
				#ifdef PARTICLE_MASS
		              particle.mass = _mass;
				#endif	
						if( long( r / Dr + 0.5 ) > 0)
						    particle.mpw = std::min(1.,Dt*timestep/InjectSmoothTime) *density*(2.0*PI*(long(r/Dr+0.5))*Dr*Dr*Dz) / (NumPartPerLine*NumPartPerLine);
						else 
						    particle.mpw =  std::min(1.,Dt*timestep/InjectSmoothTime) *density*(PI*Dr*Dr*Dz*0.25) / (NumPartPerLine*NumPartPerLine);
						
						gama = sqrt(_mass*_mass + dot(particle.PULS,particle.PULS) );	
						energyInj += particle.mpw*(gama - _mass);
						add_particle_scatter(particle);				
    }
	injectionEnergy += energyInj;
}
void ParticlesArray::source_uniform_from_bound(long timestep){
    double energyInj = 0;
	Particle particle;
	long i;//, kmax;
	double pb,gama;
	double r,z,pr, pp, pz,x,y,px,py;
	double vb = velocity;
	double t_init = temperature;
	long numInj = 4. / PI * NumPartPerCell * width / Dr;
	
	bool LeaftInj =  (vb>0. && _world.region.boundType_d1[0] == OPEN);
	bool RightInj =  (vb<0. && _world.region.boundType_d1[1] == OPEN);

	if(!( LeaftInj || RightInj )) return;

	//kmax = size();

	for(i = 0; i < numInj; ++i){

		z = Dz*Uniform01();

		x = width*Uniform01();
		y = width*Uniform01();
		
		r = sqrt(x*x+y*y);
		
		pb = vb / sqrt(1.0-vb*vb);

		px = Gauss(t_init);
		py = Gauss(t_init);

		pr = x/r * px + y/r * py; 
		pp = - y/r * px + x/r * py; 
		
		pz = pb + Gauss(t_init); 
		
		bool CondInj = r < width && z<= Dt*fabs(pz/sqrt(1. + pr*pr + pp*pp + pz * pz) );
		
		if( CondInj ) {

				particle.POS.r() = r ;
				particle.PULS.z() = pz;
				
				if(LeaftInj ){
					  particle.POS.z() = z + Dz * _world.region.dampCells_d1[0];
					  particle.PULS.p() = pp;
					  particle.PULS.r() = pr;
				}
				else if (RightInj ){
					  particle.POS.z() = Dz * _world.region.numCells_d1 - z - Dz *  _world.region.dampCells_d1[1];
					  particle.PULS.p() = - pp;
					  particle.PULS.r() = - pr;
					}
					else{
					  std::cout << "Error Inject Particles type" << std::endl; exit(0); 
				}
				
				particle.mpw = std::min(1.,Dt*timestep/InjectSmoothTime) * density * PI * Dr * Dz * width / (NumPartPerCell);
			
				#ifdef PARTICLE_MASS
		              particle.mass = _mass;
				#endif	
			
				gama = sqrt(_mass*_mass + dot(particle.PULS,particle.PULS) );
					
				energyInj += particle.mpw*(gama - _mass);
				
				add_particle_scatter(particle);			
			
		}
	}
			
	injectionEnergy += energyInj;
}
void ParticlesArray::source_focused_gauss(long timestep){
    double energyInj = 0;
    Particle particle;
	long i;//, kmax;
	double pb,gama;
	double r,r1,r2,rf,z,pr, pp, pz,x,y,px,py;
	double vb = velocity;
	double zFocus = focus;

	double sigma;
	/// sqrt(e) * sootnoshenie ploshadej* chislo chastic
	long numInj = 1.6487*4./PI*NumPartPerCell*width/Dr;

	bool LeaftInj =  (vb>0. && _world.region.boundType_d1[0] == BOUND_TYPE_OPEN);
	bool RightInj =  (vb<0.  && _world.region.boundType_d1[1] == BOUND_TYPE_OPEN);

	if(!( LeaftInj || RightInj )) return;
	
	//std::cout <<  numInj << "\n";
	for(i = 0; i < numInj; ++i){

		z = Dz * Uniform01();

		x = fabs(Gauss(width));
		y = fabs(Gauss(width));
		
		rf = sqrt(x*x+y*y);

		pb = vb / sqrt(1.0-vb*vb);

		sigma = (Rmax-width)/Lmax*pb;
		
		px = Gauss(sigma/3.);
		py = Gauss(sigma/3.);

		pz = pb; 
		
		r1 = sqrt((x + zFocus * px/pz)*(x + zFocus * px/pz)+(y + zFocus * py/pz)*(y + zFocus * py/pz));
		
		x = x - zFocus * px/pz;
		y = y - zFocus * py/pz;
		r2 = sqrt(x*x+y*y);
		r = r2;

		pr = x/r * px + y/r*py; 
		pp = - y/r * px + x/r*py; 
		
		bool InFocus = rf < 3.*width;
		bool Bound1 = r1 < 0.8 * Dr * (NumCellsR_glob - DampCellsR_glob);
		bool Bound2 = r2 < 0.8 * Dr * (NumCellsR_glob - DampCellsR_glob);
		bool CondCurr = z <= Dt*fabs(pz/sqrt(1. + pr*pr + pp*pp + pz * pz));
		
		bool CondInj =  Bound1 && Bound2 && CondCurr && InFocus;
		
		if( CondInj ) {

				particle.POS.r() = r ;
				particle.PULS.z() = pz;
					
				if(LeaftInj){
					  particle.POS.z() =  z + Dz *  _world.region.dampCells_d1[0];
					  particle.PULS.p() = pp;
					  particle.PULS.r() = pr;
				}
				else if ( RightInj){
					  particle.POS.z() = Dz * _world.region.numCells_d1 - z - Dz *  _world.region.dampCells_d1[1];
					  particle.PULS.p() = - pp;
					  particle.PULS.r() = - pr;
					}
					else{
					  std::cout << "Error Inject Particles type" << std::endl; 
					  exit(0); 
				}

				particle.mpw = std::min(1.,Dt*timestep/InjectSmoothTime)*density * PI * Dr * Dz * width / (NumPartPerCell);
				#ifdef PARTICLE_MASS
		              particle.mass = _mass;
				#endif	
				gama = sqrt(_mass*_mass + dot(particle.PULS,particle.PULS) );
				energyInj += (particle.mpw*(gama - _mass));
					
				add_particle_scatter(particle);
			
			
		}
	}
			
	injectionEnergy += energyInj;

}
void ParticlesArray::source_force_line_gauss(long timestep){
    double energyInj = 0;
    Particle particle;
	long i;//, kmax;
	double pb,gama;
	double r,r1,r2,rf,z,pr, pp, pz,x,y,px,py;
	double vb = velocity;
	//double zFocus = focus;

	double sigma;
	/// sqrt(e) * sootnoshenie ploshadej* chislo chastic
	long numInj = 1.6487*4./PI*NumPartPerCell*width/Dr;

	bool LeaftInj =  (vb>0. && _world.region.boundType_d1[0] == BOUND_TYPE_OPEN);
	bool RightInj =  (vb<0.  && _world.region.boundType_d1[1] == BOUND_TYPE_OPEN);
    static CoilsArray coils;
    double3 B;

	if(!( LeaftInj || RightInj )) return;

	
	//std::cout <<  numInj << "\n";
	for(i = 0; i < numInj; ++i){

		z = Dz * Uniform01();

		x = fabs(Gauss(width));
		y = fabs(Gauss(width));
		
		rf = sqrt(x*x+y*y);
			
		B = coils.get_B(z + Dz *  _world.region.dampCells_d1[0],rf);

		pb = vb / sqrt(1.0-vb*vb);

		pz = pb*B.z()/(mag(B));
		pr = pb*B.r()/(mag(B));
		pp = 0;
		
		bool InFocus = rf < 3.*width;
		bool Bound1 = r1 < 0.8 * Dr * (NumCellsR_glob - DampCellsR_glob);
		bool Bound2 = r2 < 0.8 * Dr * (NumCellsR_glob - DampCellsR_glob);
		bool CondCurr = z <= Dt*fabs(pz/sqrt(1. + pr*pr + pp*pp + pz * pz));
		
		bool CondInj =  Bound1 && Bound2 && CondCurr && InFocus;
		
		if( CondInj ) {

				particle.POS.r() = rf ;
				particle.PULS.z() = pz;
					
				if(LeaftInj){
					  particle.POS.z() =  z + Dz *  _world.region.dampCells_d1[0];
					  particle.PULS.p() = pp;
					  particle.PULS.r() = pr;
				}
				else if ( RightInj){
					  particle.POS.z() = Dz * _world.region.numCells_d1 - z - Dz *  _world.region.dampCells_d1[1];
					  particle.PULS.p() = - pp;
					  particle.PULS.r() = - pr;
					}
					else{
					  std::cout << "Error Inject Particles type" << std::endl; 
					  exit(0); 
				}

				particle.mpw = std::min(1.,Dt*timestep/InjectSmoothTime)*density * PI * Dr * Dz * width / (NumPartPerCell);
				#ifdef PARTICLE_MASS
		              particle.mass = _mass;
				#endif	
				gama = sqrt(_mass*_mass + dot(particle.PULS,particle.PULS) );
				energyInj += (particle.mpw*(gama - _mass));
					
				add_particle_scatter(particle);
			
			
		}
	}
			
	injectionEnergy += energyInj;

}
