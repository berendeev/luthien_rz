#include "Particles.h"
#include "Shape.h"

inline double layer_resumption_left( const Region& region){
	return Dz * (region.dampCells_d1[0] + 1);
}
inline double layer_resumption_right( const Region& region){
	return Dz * (region.numCells_d1 - region.dampCells_d1[1] - 1);
}

inline int iround(double x){
    if ( x < 0 ) x -= 0.5;
        else x += 0.5;
    return (int) x;
}

void CopyToBuf(const Particle& particleSource, Array<Particle>& ParticlesBuf, const Region& domain){
  Particle particle = particleSource;
  particle.placeInWorld(domain);
  ParticlesBuf.add(particle);
}

void addFromBuf(Array<Particle>& particlesData,Array<Particle>& ParticlesBuf, const Region& domain){
  	Particle particle;
 	while( ParticlesBuf.size() > 0){
  	   	particle = ParticlesBuf.pop();
		particle.placeInRegion(domain);
		particlesData.add(particle);
  }
}



void MPIExchangeParticles(Array<Particle>& ParticlesBufLeft, Array<Particle>& ParticlesBufRight, const MPI_Topology& MPIconf){
  const long maxSize = ParticlesBufLeft.capacity();
  static Array<Particle> RecvBufLeft(maxSize);
  static Array<Particle> RecvBufRight(maxSize);
  long guestLeft, guestRight, sizeLeft,sizeRight;
  int tag = 2;
  long k;
  long sizeParticleStruct = iround(sizeof(Particle) / sizeof(double));
  MPI_Status status;
  
	auto right = MPIconf.next_line();
  
  	auto left = MPIconf.prev_line();


	sizeLeft = ParticlesBufLeft.size();
	sizeRight = ParticlesBufRight.size();

  MPI_Sendrecv(&sizeLeft, 1, MPI_LONG, left, tag, 
                           &guestRight, 1, MPI_LONG, right, tag, 
                           MPIconf.comm_line(), &status);
  MPI_Sendrecv(&sizeRight, 1, MPI_LONG, right, tag, 
                           &guestLeft, 1, MPI_LONG, left, tag, 
                           MPIconf.comm_line(), &status);

  //MPI_Barrier(MPIconf.CommLine);
  MPI_Sendrecv(&ParticlesBufLeft(0), (sizeLeft + 1) *  sizeParticleStruct, MPI_DOUBLE_PRECISION, left, tag, 
                           &RecvBufRight(0), (guestRight + 1) *  sizeParticleStruct, MPI_DOUBLE_PRECISION, right, tag, 
                           MPIconf.comm_line(), &status);
  MPI_Sendrecv(&ParticlesBufRight(0), (sizeRight + 1) *  sizeParticleStruct, MPI_DOUBLE_PRECISION, right, tag, 
                           &RecvBufLeft(0), (guestLeft + 1) *  sizeParticleStruct, MPI_DOUBLE_PRECISION, left, tag, 
                           MPIconf.comm_line(), &status);
   // MPI_Barrier(MPIconf.CommLine);

  for(k = 0; k< guestLeft; k++){
  	ParticlesBufLeft(k) = RecvBufLeft(k);
  }
  ParticlesBufLeft.size() = guestLeft;
   
  for(k = 0; k< guestRight; k++){
      ParticlesBufRight(k) = RecvBufRight(k); 
  }
   ParticlesBufRight.size() = guestRight;

}
void push(double2& POS, double3& PULS, long q, double mass, double mpw, const Array2D<double3>& fieldE, \
		   const Array2D<double3>& fieldB, Array2D<double3>& fieldJ, double3 E = double3(0.,0.,0.) ){
  	constexpr auto SMAX = 2*SHAPE_SIZE;
	long rk, zk, n, m, indr, indz;
	double zz, rr, arg;
	double snm1,snm2,snm3,snm4,gama,sina,cosa;
	double3 B;
	double3 U,US,U2,U1,UCYL,T,C;
	double rn, zn, xn, yn;
	double a,b,tokp;
	alignas(64) double sr[SMAX];
	alignas(64) double sz[SMAX];
	alignas(64) double sdr[SMAX];
	alignas(64) double sdz[SMAX];
	alignas(64) double sr_n[SMAX];
	alignas(64) double sz_n[SMAX];
	alignas(64) double jr[SMAX][SMAX];
	alignas(64) double jp[SMAX][SMAX];
	alignas(64) double jz[SMAX][SMAX];
	const double rdr = 1. / Dr;
	const double rdz = 1. / Dz;
	const double dtp = 0.5 * Dt;
	const double conz = 0.5  / (Dt*2.*PI*Dz) * mpw;
	const double conr = 0.5  / (Dt*2.*PI*Dr) * mpw;
			
	zz = POS.z() / Dz;
	rr = POS.r() / Dr;
			
	zk = long(zz);
	rk = long(rr);
	//if(ik<0 || jk < 0 || ik>NumCellsR || jk > NumCellsZ){
		//	cout << "err " << r << " " << z << " " << pr << " " << qDens << endl;  //indx=ik-1+n+dlinax;
		//	exit(66);
		//}
	
	for(n = 0; n < SMAX; ++n){
		arg = -zz + double(zk - CELLS_SHIFT + n);
		sz[n] = Shape(arg);
		sdz[n] = Shape(arg + 0.5);
		arg = -rr + double(rk - CELLS_SHIFT + n);
		sr[n] = Shape(arg);
		sdr[n] = Shape(arg + 0.5);
	}
		
	for(n = 0; n < SMAX; ++n){
		for(m = 0; m < SMAX; ++m){
			jr[n][m] = 0.;
			jp[n][m] = 0.;
			jz[n][m] = 0.;
			
			snm1 = sdr[n] * sz[m];
			snm4 = sr[n] * sz[m];
			snm2 = sr[n] * sdz[m];
			snm3 = sdr[n] * sdz[m];
			indz = zk + m;
			indr = rk - CELLS_SHIFT + n;
			
			if(indr >= 0){			
				E += double3(snm2,snm1,snm4) * fieldE(indz,indr);
				B += double3(snm1,snm2,snm3) * fieldB(indz,indr);
			} 
			else {
				E.z() += snm2 * fieldE(indz,-indr).z();
				E.r() -= snm1 * fieldE(indz,-indr-1).r();
				E.p() += snm4 * fieldE(indz,-indr).p();
				B.z() += snm1 * fieldB(indz,-indr-1).z();
				B.r() -= snm2 * fieldB(indz,-indr).r();
				B.p() += snm3 * fieldB(indz,-indr-1).p();
			}
		}
	}

	U1 = PULS + q * dtp * E;

	a = q * dtp / sqrt(1. + dot(U1,U1) );
	T = a * B;

	b = 2. / (1. + dot(T,T));
	C = b * T;
		
	US = U1 + cross(U1,T);
		
	U2 = U1 + cross(US,C);
			
	PULS = U2 + q * dtp * E;
		
	gama = 1. / sqrt(mass * mass + dot(PULS,PULS) );
			
	U = PULS * gama;

	xn = POS.r() + Dt * U.r();
	yn = Dt * U.p();
	zn = POS.z() + Dt * U.z();
	rn = sqrt(xn*xn+yn*yn);
	if(rn!=0.){
		sina = yn / rn;
		cosa = xn / rn;
		  
	} else{
		cosa = 1.;
		sina = 0.;
	}

	UCYL.r() = U.r() * cosa + U.p() * sina;
	UCYL.p() = U.p() * cosa - U.r() * sina;
	UCYL.z() = U.z();

	gama = mass / sqrt(1.-dot(UCYL,UCYL));
	
	PULS.r() = gama * UCYL.r();
	PULS.p() = gama * UCYL.p();

	POS.r() = rn;
	POS.z() = zn;
	for(n = 0; n < SMAX; ++n){
		arg = -POS.z() * rdz + double(zk - CELLS_SHIFT + n);
		sz_n[n] = Shape(arg);
		arg = -POS.r() * rdr + double(rk - CELLS_SHIFT + n);
		sr_n[n] = Shape(arg);
	}

	gama = sqrt(mass*mass + dot(PULS,PULS));

	tokp = PULS.p() * mpw / (gama * 6.) ;

	for(n = 0; n < SMAX; ++n){

		for(m = 0; m < SMAX; ++m){
		  
			if(n == 0) jr[n][m] = -q * conr * (sr_n[n] - sr[n]) * (sz_n[m] + sz[m]);
			if(n > 0 && n < SMAX-1) jr[n][m] = jr[n-1][m] - q * conr * (sr_n[n]-sr[n])*(sz_n[m]+sz[m]);
			if(m == 0) jz[n][m] = -q * conz * (sr_n[n] + sr[n]) * (sz_n[m] - sz[m]);
			if(m > 0 && m < SMAX - 1) jz[n][m] = jz[n][m-1] - q * conz * (sr_n[n] + sr[n]) * (sz_n[m]-sz[m]);
			if(n < SMAX - 1 && m < SMAX-1) jp[n][m] = q * tokp * (sz_n[m] * (2. * sr_n[n] + sr[n]) + sz[m] * (2. * sr[n] + sr_n[n]));
			
			indr = rk - CELLS_SHIFT + n;
			indz = zk  + m;

			if(indr > 0){
				fieldJ(indz,indr).r() += (jr[n][m] / (Dr * (indr+0.5)));
				fieldJ(indz,indr).z() += (jz[n][m] / (Dr * indr));
				fieldJ(indz,indr).p() += (jp[n][m] / (2. * PI * Dr * Dr * Dz * indr));
			}
			else{
				if(indr == 0){
					fieldJ(indz,indr).r() += (2. * jr[n][m] / Dr);
					fieldJ(indz,indr).z() += (6. * jz[n][m] / Dr);
					fieldJ(indz,indr).p() = 0.;
				} else{
					if(indr==-1){
					    fieldJ(indz,0).r() -= (2. * jr[n][m] / Dr);
					    fieldJ(indz,1).z() += (jz[n][m] / Dr);
					    fieldJ(indz,1).p() += (jp[n][m] / (2*PI*Dr*Dr*Dz));
					} else{
					    fieldJ(indz,1).r() -= (1.5 * jr[n][m] / Dr );
					    fieldJ(indz,2).z() += (2. * jz[n][m] / Dr);
					    fieldJ(indz,2).p() += (jp[n][m] / (4. * PI * Dr * Dr * Dz));
					}
				  
				}
				
				  
			}
		}
	}
		
}



void ParticlesArray::move(Mesh& mesh,long timestep){
	long k, kmax;
	double phi,pxy;
	double2 POS,POS_glob;
	double3 PULS;
	Particle particle;
	bool in_area;
	bool lostZLeft, lostZRight, lostR;
	double t_init = temperature;
	double layer;
	const long ParticlesBufSize = 2*MaxSizeOfParts / ( _world.region.numCells_d1);
	static Array<Particle> ParticlesBufLeft(ParticlesBufSize);
	static Array<Particle> ParticlesBufRight(ParticlesBufSize);
	
	if (charge == 0) return;

	k=0;
	kmax = size();

	while (k < kmax ) {
		POS = particlesData(k).POS;
		PULS = particlesData(k).PULS;
		
		double3 ELas;	
		
		for (const auto& las : mesh.lasers){
			POS_glob = _world.region.get_coord_glob(POS);
			ELas += las.force(POS_glob,timestep);
		}
		push(POS,PULS, charge, mass(k), particlesData(k).mpw, 
			mesh.fieldE, mesh.fieldB, mesh.fieldJ, ELas);
		
		if( option.boundResumption == 1){
		  layer = layer_resumption_left(_world.region);
		  bool addFromLeft = _world.region.boundType_d1[0] == BOUND_TYPE_OPEN 
		  				&& particlesData(k).POS.z() <= layer && POS.z() > layer;
		  layer = layer_resumption_right(_world.region);
		  bool addFromRight = _world.region.boundType_d1[1] == BOUND_TYPE_OPEN 
		  				&& particlesData(k).POS.z() >= layer && POS.z() < layer;
		  
		  if (addFromLeft || addFromRight){
			    particle = particlesData(k);

				particle.POS.z() = (addFromLeft ? POS.z()-Dz : POS.z()+Dz);
				particle.POS.r() = POS.r();
				particle.PULS.z() = PULS.z();
				phi = 2.*PI*Uniform01();
				pxy = Gauss(t_init);
				particle.PULS.p() = pxy*sin(phi);
				particle.PULS.r() = pxy*cos(phi);
				particlesData.add(particle);
		   }
		}		
		
		particlesData(k).POS = POS;
		particlesData(k).PULS = PULS;
		k++;
	}
	

	k=0;
	while (k < size()) {

		POS = particlesData(k).POS;

		
		lostZLeft = (POS.z() < Dz*_world.region.dampCells_d1[0]);
		 
		lostZRight = (POS.z() >= Dz*(_world.region.numCells_d1 - _world.region.dampCells_d1[1]));
		
		lostR = (POS.r() >= Dr*_world.region.numCells_d2);
		
		in_area = ! (lostZLeft || lostZRight || lostR);
		
		if( in_area )  
		  ++k;
		else{
		  
		  
		  if(lostZLeft && _world.region.boundType_d1[0] == BOUND_TYPE_NEIGHBOUR){
		    CopyToBuf(particlesData(k),ParticlesBufLeft, _world.region);
		  }
		  if(lostZRight && _world.region.boundType_d1[1] == BOUND_TYPE_NEIGHBOUR){
		    CopyToBuf(particlesData(k),ParticlesBufRight, _world.region);
		  }
		  
		  particlesData.del(k);
		}
	}

	MPIExchangeParticles(ParticlesBufLeft, ParticlesBufRight,_world.MPIconf);

	addFromBuf(particlesData, ParticlesBufLeft, _world.region);
	addFromBuf(particlesData, ParticlesBufRight, _world.region);
	
}


void ParticlesArray::move_virt(Mesh& mesh,long timestep){
	long virt;
	double z, zL, zR;
	double2 POS;
	double3 PULS;
	if(charge == 0) return;
	if ( _world.region.boundType_d1[0] != BOUND_TYPE_OPEN && _world.region.boundType_d1[1] != BOUND_TYPE_OPEN) return;
	  
	long k=0;

	while (k < particlesData.size() ) {
		virt = 0;
		z = particlesData(k).POS.z();
		zL = Dz*(_world.region.dampCells_d1[0]) ;
		zR = Dz*(_world.region.numCells_d1 - _world.region.dampCells_d1[1]);

		if(z < zL + 2.*Dz &&  _world.region.boundType_d1[0] == BOUND_TYPE_OPEN){
			z = z - 2*Dz;
			//z = 2*zL-z;
			virt = 1;
		}
		if(z > zR - 2.*Dz  &&  _world.region.boundType_d1[1] == BOUND_TYPE_OPEN){
			z = z + 2*Dz;
			//z = 2*zR -z;
			virt = 1;
		}
		
		if(virt != 0){
			POS.z() = z;
			POS.r() = particlesData(k).POS.r();
			PULS = particlesData(k).PULS;
			push(POS,PULS, charge, mass(k), particlesData(k).mpw,
		 		mesh.fieldE, mesh.fieldB, mesh.fieldJ);
		}
		
		k++;
	}
}
