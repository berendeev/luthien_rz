#ifndef PARTICLES_H_
#define PARTICLES_H_
#include "World.h"
#include "Vec.h"
#include "Mesh.h"
#include <functional>
#include <assert.h>

// Base particle class
class ParticleSimple{
public:
	double2 POS; /*particle coordinate z,r*/
	double3 PULS; /*particle pulse pz,pr,pp*/
	double mpw;  /*macroparticle weight*/
	friend std::ostream& operator<<(std::ostream& out, const ParticleSimple &particle);
    // convert coordinate from local to global region
    void placeInWorld(const Region& domain){
        POS.z() += domain.origin;
    }
    // convert coordinate from global to local region
    void placeInRegion(const Region& domain){
        POS.z() -= domain.origin;      
    }
};

// Particle have individual mass
struct ParticleMass : ParticleSimple{
	double mass;
  	friend std::ostream& operator<<(std::ostream& out, const ParticleMass &particle);

};

// In simulation we use a Particle class
// Define whitch class you need
#ifdef PARTICLE_MASS
typedef ParticleMass Particle;
#else
typedef ParticleSimple Particle;
#endif

// Option class for Array of particles
class ParticlesOption{
public:
    long boundResumption;
    long sourceType;
    long smoothMass;
    double smoothMassSize;
    double smoothMassMax; 

};

// Array of particles with the same properties
class ParticlesArray{

public:
    Array<Particle> particlesData;

    Array2D<double> densityOnGrid;
    Array2D<double> phaseOnGrid;
    static long counter;

    long charge;
    double mpw;             /*macroparticle weight*/
    double density;
    double phasePZmin, phasePZmax;
    double pot_I;
    double pot_k;
    double kineticEnergy;
    double injectionEnergy;
    std::string name;
    double temperature;
    double velocity;
    double width;
    double focus;
    ParticlesOption option;
    std::string initDist;
    std::vector<double> distParams;
    ParticlesArray(const std::vector<std::string>& vecStringParams,World& world);
    void set_params_from_string(const std::string& line);

    void inject(long timestep);
    void update(Mesh& mesh,long timestep);
    // get particle from array
    Particle& operator() (long i) {
        return particlesData(i);
    }
    // get particle from array
    const Particle& operator() (long i) const{
        return particlesData(i);
    }
    // get size of particles array
    long size() const{
        return particlesData.size();
    }
    double mass(long k) const{
    	#ifdef PARTICLE_MASS
          return particlesData(k).mass;
        #else
          return _mass;
        #endif  
    }
    double mass() const{
          return _mass;
    }
    void add_particle_scatter(const Particle& particle){
        if(counter % _world.MPIconf.size_depth() == _world.MPIconf.rank_depth() )
            particlesData.add(particle);
        ++counter;
    }
    /*Methods for set distribution af particles*/
    void set_distribution();
    void set_distribution_density(std::function<double(double2 )> set_density);
    void set_smooth_mass();

    double get_kinetic_energy() const;
    double get_inject_energy() const{
        return injectionEnergy;
    }
    void density_on_grid_update();
    void phase_on_grid_update();
    void move(Mesh& mesh,long timestep);
    void move_virt(Mesh& mesh,long timestep);

    void set_space_distribution();
    void set_pulse_distribution();
    void set_strictUniformRZ(long startZ, long endZ, long endR, const Region& domain, const MPI_Topology& MPIconf);
    void set_strictCosRZ(long startZ, long endZ, long endR, double delta_n0, double k0, const Region& domain, const MPI_Topology& MPIconf);
    void set_under_force_line(long startZ, long endZ, long endR, double zf, double rf, const Region& domain, const MPI_Topology& MPIconf);
    void source_uniform_from_bound(long timestep);
    void source_uniform_from_bound_mpw(long timestep);
    void source_focused_gauss(long timestep);
    void source_force_line_gauss(long timestep);

    void read_from_recovery(const MPI_Topology &MPIconf);
    void write_recovery(const MPI_Topology &MPIconf) const;


/* Reference to world. Particles can change of currents and another parameters*/
protected:
    World &_world;
    double _mass;

};

double PulseFromKev(double kev, double mass);

int get_num_of_type_particles(const std::vector<ParticlesArray> &Particles, const std::string& ParticlesType);
void collision( const Mesh &mesh, const World& world ,std::vector<ParticlesArray> &Particles,long timestep);


#endif 
