#include "Vec.h"
#include "World.h"
#include "Particles.h"
#include "Shape.h"

double PulseFromKev(double kev, double mass){
  double gama = kev / MC2 + mass;
  return sqrt((gama*gama)- mass);
}

long ParticlesArray::counter;

ParticlesArray::ParticlesArray(const std::vector<std::string>& vecStringParams, World& world):
    particlesData(MaxSizeOfParts),
    densityOnGrid(world.region.nn),phaseOnGrid(PzMax + ADD_NODES,PpMax + ADD_NODES),
    _world(world) 
    {
    for (const auto& line: vecStringParams){
        set_params_from_string(line);
    }
    injectionEnergy = 0.;

    if (RECOVERY){ 
        read_from_recovery(world.MPIconf);
    }else{
        set_distribution();
    }

    density_on_grid_update();
    phase_on_grid_update(); 
}

// Устанавливаем значения основных параметров в переменной Params в соответствии с содержимым сроки
void ParticlesArray::set_params_from_string(const std::string& line){
    std::vector<std::string> strvec;

    strvec = split(line, ' ');

    if(strvec[0]=="Particles") {
        this->name = strvec[1];
    }

    if(strvec[0]=="Temperature"){
        temperature = stod(strvec[1]);
    }

    if(strvec[0]=="Mass"){
        this->_mass = stod(strvec[1]);
    }

    if(strvec[0]=="Charge"){
       this->charge =  stol(strvec[1]);
    }
    if(strvec[0]=="SmoothMass"){
       option.smoothMass =  stol(strvec[1]);
    }
    if(strvec[0]=="SmoothMassSize"){
       option.smoothMassSize =  stod(strvec[1]);
    }
    if(strvec[0]=="SmoothMassMax"){
       option.smoothMassMax =  stod(strvec[1]);
    }        
    if(strvec[0]=="Width"){
        width = stod(strvec[1]);
    }
    if(strvec[0]=="Density"){
        this->density = stod(strvec[1]); 
        this->mpw = density*( PI * Dr * Dr * Dz * PlasmaCellsR_glob / double(NumPartPerCell));
    }
    if(strvec[0]=="Focus"){
       focus = stod(strvec[1]);
    }
    if(strvec[0]=="Velocity"){
        velocity = stod(strvec[1]);
    }
    if(strvec[0]=="Pot_I"){
        this->pot_I = stod(strvec[1]);
    }
    if(strvec[0]=="Pot_k"){
        this->pot_k = stod(strvec[1]);
    }

    if(strvec[0]=="Pz_min"){
        this->phasePZmin = stod(strvec[1]);
    }

    if(strvec[0]=="Pz_max"){
         this->phasePZmax = stod(strvec[1]);
    }
    if (strvec[0]=="DistParams"){
        if(strvec[1]=="UniformCosZ_dn_k"){
            initDist = strvec[1];
            distParams.push_back(stod(strvec[2]));
            distParams.push_back(stod(strvec[3]));
        }
        if(strvec[1]=="Uniform"){
            initDist = strvec[1];
        }
        if(strvec[1]=="StrictUniform"){
            initDist = strvec[1];
        }
        if(strvec[1]=="UnderForceLine_Z_R"){
            initDist = strvec[1];
            distParams.push_back(stod(strvec[2]));
            distParams.push_back(stod(strvec[3]));
        }
        if(strvec[1]=="None"){
           initDist = strvec[1];
        }
    }
    if(strvec[0]=="BoundResumption"){
        option.boundResumption = stod(strvec[1]);
    }
    if(strvec[0]=="SourceType"){
        option.sourceType = stod(strvec[1]);
    }
}


std::ostream& operator<<(std::ostream& out, const ParticleSimple& particle){
	out << particle.POS << " "<< particle.PULS << " " << particle.mpw;
	return out;
} 
std::ostream& operator<<(std::ostream& out, const ParticleMass& particle){
    out << particle.POS << " "<< particle.PULS << " " << particle.mpw << " " << particle.mass;
	return out;
} 





int get_num_of_type_particles(const std::vector<ParticlesArray> &Particles, const std::string& ParticlesType){
  for (int i = 0; i < NumOfPartSpecies; ++i){
    if(Particles[i].name == ParticlesType) return i;
  }
  return -1;
  //std::cout << "Incorrected Particles Name/Species. Exit " << std::endl;
  //exit(1);
}

void MPIExchangeDensity(Array2D<double>& dens, const MPI_Topology& MPIconf){

  int tag = 1;
  MPI_Status status;
  int bufSize = ADD_NODES*dens.size_d2();
  long sendIndx; 
  static double *recvBuf = new double[bufSize];
  
   auto right = MPIconf.next_line();
  
    auto left = MPIconf.prev_line();


  sendIndx = (dens.size_d1()-ADD_NODES)*dens.size_d2();
  // fromRight to Left
  MPI_Sendrecv(&dens.data(sendIndx), bufSize, MPI_DOUBLE_PRECISION, right, tag, 
                           recvBuf, bufSize, MPI_DOUBLE_PRECISION, left, tag, 
                           MPIconf.comm_line(), &status);
  
  for (long i = 0; i< bufSize; i++){
    dens.data(i)+= recvBuf[i];
  }
  
  MPI_Sendrecv(&dens.data(0), bufSize, MPI_DOUBLE_PRECISION,left , tag, 
                           &dens.data(sendIndx), bufSize, MPI_DOUBLE_PRECISION, right, tag, 
                           MPIconf.comm_line(), &status);
  
  MPI_Barrier(MPIconf.comm_line());
  
}

void ParticlesArray::density_on_grid_update(){ 
    constexpr auto SMAX = 2*SHAPE_SIZE;
    long i, j, rk, zk, k, n,m;
    double sr[SMAX];
    double sz[SMAX];
    double arg;
    Particle particle;   
    long indr, indz;
    const long size_d1 = densityOnGrid.size_d1();
    const long size_d2 = densityOnGrid.size_d2();

    densityOnGrid.clear();

    for ( k = 0; k < particlesData.size(); ++k){
        particle = particlesData(k);
        zk = long(particle.POS.z() / Dz);
        rk = long(particle.POS.r() / Dr);

        for(n = 0; n < SMAX; ++n){
            arg = -particle.POS.r() / Dr + double(rk - CELLS_SHIFT + n);
            sr[n] = Shape(arg);
            arg = -particle.POS.z() / Dz + double(zk - CELLS_SHIFT + n);
            sz[n] = Shape(arg);
        }

        for(n = 0; n < SMAX; ++n){
            indz = zk + n;
            for(m = 0; m < SMAX; ++m){
                indr = rk - CELLS_SHIFT + m;
                densityOnGrid(indz,std::abs(indr)) += particle.mpw * sr[m] * sz[n];
            }
        }
    }
    for (j = 0; j < size_d1; ++j) {
        densityOnGrid(j,0) /= (PI*Dr*Dr*Dz/3.0); 
      }
      
    for(i = 0; i < size_d1; ++i){
      for (j = 1; j < size_d2; ++j) {
        densityOnGrid(i,j) /= (2.0*PI*j*Dr*Dr*Dz); 
      }
      
    }

    MPI_Allreduce ( MPI_IN_PLACE, &densityOnGrid.data(0), size_d1*size_d2, MPI_DOUBLE, MPI_SUM, _world.MPIconf.comm_depth());
    MPI_Barrier(MPI_COMM_WORLD);

    MPIExchangeDensity(densityOnGrid, _world.MPIconf);

}

void ParticlesArray::phase_on_grid_update(){ 
    long size_z = phaseOnGrid.size_d1();
    long size_p = phaseOnGrid.size_d2();
    long i, j, pk, zk, k;
    double z, pz;
    bool blounder;
    Particle particle;
    double z_min = Dz*(_world.region.dampCells_d1[0]);
    double z_max = Dz*(_world.region.numCells_d1 - _world.region.dampCells_d1[1]);
    double pdz = (z_max - z_min) / PzMax;
    double pdp = (phasePZmax -phasePZmin) / PpMax;

    for(i=0; i< size_z; i++){
        for(j=0; j < size_p; j++){
            phaseOnGrid(i,j) = 0;
        }
    }
    for (k=0;k<particlesData.size();k++){
        particle = particlesData(k);   
        z = particle.POS.z();
        pz = particle.PULS.z();

        zk = long((z-z_min) / pdz); 
        pk = long((pz-phasePZmin) / pdp);

        blounder = (zk<0) || (zk>=PzMax) || (pk<0) || (pk>=PpMax);
        if(!blounder){
            phaseOnGrid(zk,pk) += (particle.mpw / (pdz*pdp) );
        }

    }


    MPI_Allreduce ( MPI_IN_PLACE, &phaseOnGrid.data(0), size_z*size_p, MPI_DOUBLE, MPI_SUM, _world.MPIconf.comm_depth());
    MPI_Barrier(MPI_COMM_WORLD);

}


void ParticlesArray::set_distribution(){
	set_space_distribution();
	set_pulse_distribution();
}

void ParticlesArray::update(Mesh& mesh,long timestep){
    mesh.fieldB += mesh.fieldB0;
    
    move_virt(mesh,timestep);
    move(mesh,timestep);

    if (timestep % TimeStepDelayDiag2D == 0){
        density_on_grid_update();
        phase_on_grid_update();
    }
    
    mesh.fieldB -= mesh.fieldB0;
}
void ParticlesArray::inject(long timestep){
    switch(option.sourceType){
       case SOURCE_UNIFORM_MPW:
            source_uniform_from_bound_mpw(timestep);
            break;
       case SOURCE_UNIFORM:
            source_uniform_from_bound(timestep);
            break;
        case SOURCE_FOCUSED_GAUSS:
            source_focused_gauss(timestep);
            break;
        case SOURCE_NONE:
            break;
        case SOURCE_FORCE_LINE:
            source_force_line_gauss(timestep);
            break;
        default:
            break;

        }
}
double ParticlesArray::get_kinetic_energy() const{
    double energy = 0;
    double3 PULS;
    double gama;

    for(auto k = 0; k < particlesData.size(); ++k){
        PULS = particlesData(k).PULS;
        gama = sqrt(mass(k)*mass(k) + dot(PULS,PULS) );
        energy += particlesData(k).mpw * (gama - mass(k));
    }   

    return energy;
}
