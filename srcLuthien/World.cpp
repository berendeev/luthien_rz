#include "World.h"
#ifdef RANDOMSTD
  #include <random>

  std::mt19937 gen;
  std::uniform_real_distribution<> urd(0, 1); 

  double Uniform01(){
    return urd(gen);
  }

  void SetRandSeed(int val){
    gen.seed(val);
  }
#else
  double Uniform01(){
    return (double)(rand())/RAND_MAX ;
  }
  void SetRandSeed(int val){
      srand(val);
  }
#endif

double Gauss(double sigma){
  double r1 = Uniform01();
  double r2 = Uniform01();
  
  return sigma*sqrt(-2.0*log(r1))*sin(2.0*PI*r2);
}


void MPI_Topology::set_topology(){
  
    MPI_Comm_size(MPI_COMM_WORLD,&_size);
    MPI_Comm_rank(MPI_COMM_WORLD,&_rank);
    
    if( _size % NumAreas != 0 ){
        std::cout<< "Invalid number of subdomains for decomposition! \n";
        exit(-1);
    }

    _sizeDepth = _size / NumAreas;
    _sizeLine = NumAreas;
    
    int colorLine = _rank / NumAreas;
    MPI_Comm_split(MPI_COMM_WORLD, colorLine, _rank, &_commLine);

    int colorDepth = _rank % NumAreas;
    MPI_Comm_split(MPI_COMM_WORLD, colorDepth, _rank, &_commDepth);

    MPI_Comm_rank(_commDepth, &_rankDepth);
    MPI_Comm_rank(_commLine, &_rankLine);
    SetRandSeed(_rank*3+1);

    if( is_master() )
        std::cout << "There are " << NumAreas << " subdomains" << std::endl;

    MPI_Barrier(MPI_COMM_WORLD);
}

// Читаем данные из файла параметров, распознаём строки и записываем данные в Params
Region::Region(){

    dz = Dz;
    dr = Dr;
    origin = 0.0;
   
    numCells_d1 = NumCellsZ_glob;
    numCells_d2 = NumCellsR_glob;    
    nd1 = numCells_d1 + ADD_NODES;
    nd2 = numCells_d2 + ADD_NODES;    
    nn(0) = nd1;
    nn(1) = nd2;
    dampCells_d1[0] = DampCellsZ_glob[0];
    dampCells_d1[1] = DampCellsZ_glob[1];    
    dampCellsForce_d1[0] = DampCellsZP_glob[0];
    dampCellsForce_d1[1] = DampCellsZP_glob[1];
    dampCells_d2 = DampCellsR_glob;
    boundType_d1[0] = BoundTypeZ_glob[0];
    boundType_d1[1] = BoundTypeZ_glob[1];
    boundType_d2 = BoundTypeR_glob;

}


Region split_region(const Region& regionGlob, int rank, int splitSize){
    Region region = regionGlob;
    
    region.numCells_d1 = PlasmaCellsZ_glob / splitSize;
    region.origin = Dz * rank * region.numCells_d1;
    
    if (rank != 0) {
        region.boundType_d1[0] = BOUND_TYPE_NEIGHBOUR;
        region.dampCells_d1[0] = 0;
        region.dampCellsForce_d1[0] = 0;
        region.origin += Dz*DampCellsZ_glob[0];
    } else{
        region.numCells_d1 += DampCellsZ_glob[0];
    }
    if (rank != splitSize - 1) {
        region.boundType_d1[1] = BOUND_TYPE_NEIGHBOUR;
        region.dampCells_d1[1] = 0;
        region.dampCellsForce_d1[1] = 0;

    } else{
        region.numCells_d1 += DampCellsZ_glob[1];
    }
    region.nd1 = region.numCells_d1 + ADD_NODES;
    region.nd2 = region.numCells_d2 + ADD_NODES;
    region.nn(0) = region.nd1;
    region.nn(1) = region.nd2;    
    std::cout << "Current number of CellsZ = "<< region.numCells_d1 << ". Start coord =" << region.origin << std::endl;
    return region;
}

long MPI_Topology::accum_sum_line(long value) const{
    long sum = 0;
    int prev, next;
    int id = 1;
    MPI_Status status;
    if (NumAreas == 1) return 0;
    prev = _rankLine - 1;
    next = _rankLine  + 1;
    if( _rankLine == 0){
        prev = MPI_PROC_NULL; 

    }
    else if(_rankLine == _sizeLine - 1){
        next = MPI_PROC_NULL;
    }
    
    MPI_Recv(&sum, 1, MPI_LONG, prev, id, _commLine, &status);
    
    if( _rankLine != 0){
        value += sum; 
    }
    MPI_Send(&value, 1, MPI_LONG, next, id,  _commLine);
    return sum;

}

