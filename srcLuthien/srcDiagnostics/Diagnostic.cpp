#include "Diagnostic.h"

void Writer::output(long timestep){
    diagData.calc_radiation_Pointing(_mesh,_world.region);
    diagData.calc_fields_avg_line(_mesh,_world,timestep);
    diagData.calc_radiation_Pointing_avg(_mesh,_world.region,timestep);

    if (timestep % TimeStepDelayDiag2D == 0){
        write_particles2D(timestep);
        write_fields2D(_mesh.fieldE, _mesh.fieldB,timestep);
    }

    if (timestep % 2 == 0){

      diag_zond(timestep);
      diag_zond_lineR_bin(_mesh.fieldE, _mesh.fieldB, timestep);
      diag_zond_lineZ_bin(_mesh.fieldE, _mesh.fieldB, timestep);
    }

    if (timestep % TimeStepDelayDiag1D == 0){

        write_radiation_line(timestep);
        write_radiation_avg_line(timestep);
        diagData.calc_energy(_mesh,_species);
        write_energies(timestep);
        diagData.clear();              
    }

    if (timestep % RecTimeStep == 0 && timestep != StartTimeStep){
        for (const auto& sp: _species){
            sp.write_recovery(_world.MPIconf);
        }
        _mesh.write_recovery(_world.MPIconf);
    }

}


void DiagData::calc_fields_avg_line(const Mesh& mesh,const World& world, long timestep){
  long i,j,t;
  long sizeT = long(2 * PI / Dt);
  t = timestep % sizeT;
  auto size_z = mesh.fieldE.size_d1();
  auto size_r = mesh.fieldE.size_d2();

    
    j = zondRadR / Dr;  
    for (i = 0; i < size_z - ADD_NODES;++i){
      fieldEAvgLine.top(i,t) = mesh.get_fieldE_in_cell(i,j);
      fieldBAvgLine.top(i,t) = mesh.get_fieldB_in_cell(i,j);
    }
  
    
    i = (zondRadZ[0] - world.region.origin) / Dz; 
    if (! world.region.in_region(zondRadZ[0])) return;
    for (j = 0; j < size_r - ADD_NODES; ++j){
      fieldEAvgLine.left(j,t) = mesh.get_fieldE_in_cell(i,j);
      fieldBAvgLine.left(j,t) = mesh.get_fieldB_in_cell(i,j);
    }

    
  i = (zondRadZ[1] - world.region.origin) / Dz;
  if (!world.region.in_region(zondRadZ[1])) return;
    for (j = 0; j < size_r - ADD_NODES; ++j){
      fieldEAvgLine.right(j,t) = mesh.get_fieldE_in_cell(i,j);
      fieldBAvgLine.right(j,t) = mesh.get_fieldB_in_cell(i,j);
    }

}

void make_folders(){
    mkdir(".//Fields", S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    mkdir(".//Fields//Diag2D", S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    mkdir(".//Fields//Diag1D", S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    mkdir(".//Recovery", S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    mkdir(".//Recovery//Fields", S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    mkdir(".//Recovery//Particles", S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    mkdir(".//Anime", S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    mkdir(".//Particles", S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    mkdir(".//Performance", S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    std::cout << "Create folders for output...\n";
}

Writer::Writer(const World &world, const Mesh &mesh,std::vector<ParticlesArray> &species) : 
    _world(world),_mesh(mesh),_species(species),diagData(world.region) {
  if( !_world.MPIconf.is_master() ) return; 
  
  make_folders(); 
  for( const auto &sp : _species){
    mkdir((".//Particles//" + sp.name).c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    mkdir((".//Particles//" + sp.name+"//Diag2D").c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
  }

  fDiagEnergies = fopen("Energies.dat", "w");
}

void write_array2D(const Array2D<double>& data, long size1, long size2, const char* filename, const MPI_Topology& MPIconf){
    MPI_File fData2D;
    MPI_Status status;
    //long size1 = data.size_d1() - ADD_NODES;
    //long size2 = data.size_d2();
    long sumSize1;
    float info;
    int sizeData = size1*size2;
    
    if ( !MPIconf.is_master_depth() ) return;

    float* floatData = new float[size1*size2];

    MPI_File_open(MPIconf.comm_line(), filename, MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &fData2D);
    
    for( auto i = 0; i < size1; ++i ){
       for( auto j = 0; j < size2; ++j ){
          floatData[i*size2 + j] = float(data(i,j));
      }
    }


    sumSize1 = MPIconf.accum_sum_line(size1);

    if( MPIconf.is_last_line() ){
          info = float(sumSize1 + size1);
          MPI_File_write_at(fData2D, 0,&info, 1,MPI_FLOAT, &status);
          info = float(size2);
          MPI_File_write_at(fData2D, sizeof(float),&info, 1,MPI_FLOAT, &status);
    }
    long startWrite = sumSize1 * size2 * sizeof(float) + 2 * sizeof(float);
        
    MPI_File_write_at(fData2D, startWrite, floatData, sizeData, MPI_FLOAT, &status);
    MPI_File_close(&fData2D);
  
  delete[] floatData;

}
