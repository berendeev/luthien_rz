#ifndef DIAGNOSTIC_H_
#define DIAGNOSTIC_H_
#include "World.h"
#include "Mesh.h"
#include "Particles.h"
#include "Timer.h"
void write_array2D(const Array2D<double>& data, long size1, long size2, const char* filename, const MPI_Topology& MPIconf);

template <typename T>
struct BoundData{
    T left;
    T right;
    T top;
    BoundData(){
        std::cout<< "Error! You need template specifications for this type!\n";
    };
    BoundData(const Region &region){
        std::cout<< "Error! You need template specifications for this type!\n";
    };
    BoundData(const Region &region, long n){
        std::cout<< "Error! You need template specifications for this type!\n";
    };
    ~BoundData(){};
    void clear(){
        left = 0.;
        right = 0.;
        top = 0.;
    };
};
template<>
inline BoundData<Array1D<double> >::BoundData(const Region &region): left(region.nd2),right(region.nd2),top(region.nd1){
        clear();
}
template<>
inline BoundData<Array2D<double> >::BoundData(const Region &region, long n): left(region.nd2,n),
                right(region.nd2,n),top(region.nd1,n){
        clear();
}
template<>
inline void BoundData<Array2D<double3> >::clear(){
        left = double3(0.,0.,0.);
        right = double3(0.,0.,0.);
        top = double3(0.,0.,0.);
}

template<>
inline BoundData<Array2D<double3> >::BoundData(const Region &region, long n): left(region.nd2,n),
                right(region.nd2,n),top(region.nd1,n){
        clear();
}
template<>
inline BoundData<double>::BoundData(){
        clear();
};
template<>
inline BoundData<double3>::BoundData(){
        clear();
};

struct DiagData{

    
	DiagData(const Region &region): powerRadLine(region),
                            powerRadAvgLine(region,long(2*PI/Dt)),
                            fieldEAvgLine(region,long(2*PI/Dt)),
                            fieldBAvgLine(region,long(2*PI/Dt)){
       clear() ;
    };

	void calc_energy(const Mesh &mesh,const std::vector<ParticlesArray> &species);
	void calc_radiation_Pointing(const Mesh &mesh, const Region &region);
    void calc_fields_avg_line(const Mesh& mesh,const World& world,long timestep);
    void calc_radiation_Pointing_avg(const Mesh &mesh,const Region &region,long timestep);
    
    void clear(){
        powerRad.clear();
        powerRadAvg.clear();
        powerRadLine.clear();
	};

    BoundData<double> powerRad;
    BoundData<double> powerRadAvg;
    BoundData< Array1D<double> > powerRadLine;
    BoundData< Array2D<double> > powerRadAvgLine;
    BoundData< Array2D<double3> > fieldEAvgLine;
    BoundData< Array2D<double3> > fieldBAvgLine;

    std::map<std::string,double> energyParticlesKinetic;
    std::map<std::string,double> energyParticlesInject;
    std::map<std::string,double> energyParticlesInjectPrev;

    double energyFieldE, energyFieldB;

};


struct Writer{
protected:
    const World &_world;
    const Mesh &_mesh;
    const std::vector<ParticlesArray> &_species;
public:
	FILE *fDiagEnergies;
    DiagData diagData;

 	Writer(const World &world, const Mesh &mesh,std::vector<ParticlesArray> &species);

 	void output( long timestep);
 	~Writer(){
	 	if( !_world.MPIconf.is_master() ) return;
	        fclose(fDiagEnergies);  
	} 

    void write_particles2D(long timestep);
    void write_energies(long timestep);
    void write_radiation_line(long timestep);
    void write_radiation_avg_line(long timestep);

    void diag_zond(long timestep);
    void diag_zond_lineZ_bin(const Array2D<double3>& fieldE, const Array2D<double3>& fieldB,long timestep);
    void diag_zond_lineR_bin(const Array2D<double3>& fieldE, const Array2D<double3>& fieldB, long timestep);
    void write_fields2D(const Array2D<double3>& fieldE, const Array2D<double3>& fieldB,  const long& timestep);

};

#endif 	
