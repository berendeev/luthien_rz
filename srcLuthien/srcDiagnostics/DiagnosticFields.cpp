#include "Diagnostic.h"

/*void RWriter::Diag_Fields(const World& world,long timestep){
  
  if( timestep%1 == 0 ){
      DiagZond(world.fieldE, world.fieldB,world.MPIconf, world.region, timestep);
      DiagZondLineR(world.fieldE, world.fieldB,world.MPIconf, world.region, timestep);
      DiagZondLineZ(world.fieldE, world.fieldB, world.MPIconf, world.region,timestep);
  }
  

  if( timestep%TimeDelay == 0 ){
      Diag_Fields2D(world.fieldE, world.fieldB,timestep, world.MPIconf);
  }
}*/

void Writer::diag_zond(long timestep){
  if (!_world.MPIconf.is_master_depth()) return;
  
  char filename[50];
  static FILE *fZond; 
  long n,i,j;
  double zz,rz;  
  int numZonds = zondZ[0];
  double3 E,B;


  if (timestep == StartTimeStep){
    sprintf(filename, "./Fields/Zond%03d.dat",_world.MPIconf.rank_line() );
    fZond = fopen(filename, "w");
    fprintf(fZond, "%s ", "## timestep ");
    for (n = 0; n < numZonds; ++n){
      
      if( ! _world.region.in_region(zondZ[n]) ) continue;
      zz = zondZ[n] - Dz*DampCellsZ_glob[0];
      rz = zondR[n];
      fprintf(fZond, "%s%g%s%g%s %s%g%s%g%s %s%g%s%g%s %s%g%s%g%s %s%g%s%g%s %s%g%s%g%s ", "Ez(",zz,",",rz,")","Er(",zz,",",rz,")","Ep(",zz,",",rz,")","Bz(",zz,",",rz,")","Br(",zz,",",rz,")","Bp(",zz,",",rz,")"  );
    }
    fprintf(fZond, "\n");
  }
  
    fprintf(fZond, "%g ",Dt*timestep);
    
    for (n = 1; n <= numZonds; ++n){
      if( ! _world.region.in_region(zondZ[n]) ) continue;
      zz = zondZ[n] -  _world.region.origin;
      rz = zondR[n];
      j = long(rz / Dr);
      i = long(zz / Dz);
      E = _mesh.get_fieldE_in_cell(i,j);
      B = _mesh.get_fieldB_in_cell(i,j);
      fprintf(fZond, "%g %g %g %g %g %g ",  E.z(), E.r(), E.p(), B.z(), B.r(), B.p() );
    }
    
    fprintf(fZond, "\n");
    if(  timestep % TimeStepDelayDiag1D == 0){
      fflush(fZond);
    }

}
/*void RWriter::DiagZondLineZbin(const Array2D<double3>& fieldE, const Array2D<double3>& fieldB, const MPI_Topology& MPIconf, const Region& domain, long timestep){
  
  if (!MPIconf.is_master_depth()) return;
  
  char filename[100];
  static FILE *fZondLineZbin; 
  long skip = 1;
  float temp;
  
  if( rZondLine < 0) return;
    
  long j = rZondLine / Dr;
 
  sprintf(filename, "ZondLineTop%03d.bin",MPIconf.rank_line());

  if( timestep == StartTimeStep){
    fZondLineZbin = fopen(filename, "wb");
    temp = float( (domain.numCells_d1 - domain.dampCells_d1[0] - domain.dampCells_d1[1] - ADD_NODES) / skip);   
    fwrite(&temp, sizeof(float), 1, fZondLineZbin);
  }

  for( long i = domain.dampCells_d1[0]; i < domain.numCells_d1 - domain.dampCells_d1[1]; i += skip){
    temp = float(fieldE(i,j).z() );    
    fwrite(&temp, sizeof(float), 1, fZondLineZbin);
    temp = float(fieldE(i,j).r() );    
    fwrite(&temp, sizeof(float), 1, fZondLineZbin);
    temp = float(fieldE(i,j).p() );    
    fwrite(&temp, sizeof(float), 1, fZondLineZbin);
    temp = float(fieldB(i,j).z() );    
    fwrite(&temp, sizeof(float), 1, fZondLineZbin);
    temp = float(fieldB(i,j).r() );    
    fwrite(&temp, sizeof(float), 1, fZondLineZbin);
    temp = float(fieldB(i,j).p() );    
    fwrite(&temp, sizeof(float), 1, fZondLineZbin);
  }
  
}
*/
static void write_zond_lineR_bin(const Array2D<double3>& fieldE, const Array2D<double3>& fieldB, long indr,
      const MPI_Topology& MPIconf, const Region& domain, MPI_File& fZondLine, long currentStep,long timestep){
    
    MPI_Status status;
    char filename[50];
    auto numCells = (domain.numCells_d1 - domain.dampCells_d1[0] - domain.dampCells_d1[1]) ;
    auto sizeData = 6*numCells;
    long indz;
    
    static float* floatData = new float[6*sizeData];
   

    if( timestep == StartTimeStep){
        sprintf(filename, "./Fields/ZondLineR_node%04d.bin",(int)indr);
        MPI_File_open(MPIconf.comm_line(), filename, MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &fZondLine);
        float info = float(numCells*MPIconf.size_line() );
    
        if(MPIconf.is_first_line()){   
            MPI_File_write_at(fZondLine, 0, &info, 1, MPI_FLOAT, &status);
        }
    }

    for( auto i = 0; i < numCells; ++i){
        indz = i + domain.dampCells_d1[0];
        floatData[6 * i    ] = float(fieldE(indz,indr).z() );
        floatData[6 * i + 1] = float(fieldE(indz,indr).r() );
        floatData[6 * i + 2] = float(fieldE(indz,indr).p() );
        floatData[6 * i + 3] = float(fieldB(indz,indr).z() );
        floatData[6 * i + 4] = float(fieldB(indz,indr).r() );
        floatData[6 * i + 5] = float(fieldB(indz,indr).p() );
    }
  
    int startWrite = MPIconf.rank_line()*sizeData*sizeof(float) + sizeof(float);
  
    int sizeWrite = MPIconf.size_line()*sizeData*sizeof(float);
    
    startWrite += sizeWrite*currentStep;
    
    MPI_File_write_at(fZondLine, startWrite, floatData, sizeData, MPI_FLOAT, &status);
      
    currentStep++;
    //delete[] floatData;
}

void Writer::diag_zond_lineR_bin(const Array2D<double3>& fieldE, const Array2D<double3>& fieldB, long timestep){
  auto numZonds = zondLineR[0];
  if (!_world.MPIconf.is_master_depth() ||  numZonds <= 0) return;
  
  long indr;
  static MPI_File fZondLine[5];
  static long currentStep[5] = {0,0,0,0,0}; 


  assert (numZonds < 5 && "Number of files less then number of zonds!\n");
   
  for (auto n = 0; n < numZonds; ++n ){
    indr = long( zondLineR[n+1] / Dr);
    write_zond_lineR_bin(_mesh.fieldE, _mesh.fieldB, indr, _world.MPIconf, _world.region, fZondLine[n], currentStep[n],timestep);
    currentStep[n]++;
  }
  
}

static void write_zond_lineZ_bin(const Array2D<double3>& fieldE, const Array2D<double3>& fieldB, long indz,
               const MPI_Topology& MPIconf, const Region& domain, MPI_File& fZondLine, long currentStep,long timestep){
    
    MPI_Status status;
    char filename[50];
    int numCells = (domain.numCells_d2 - domain.dampCells_d2) ;
    auto sizeData = 6*numCells;

    std::cout<<sizeData<<"\n";
    static float* floatData = new float[sizeData];

    //static long currentStep = 0; 

    if( timestep == StartTimeStep){
        sprintf(filename, "./Fields/ZondLineZ_node%04d.bin",(int)indz + int(domain.origin/Dz) );

        MPI_File_open(MPIconf.comm_line(), filename, MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &fZondLine);
        float info = float(numCells);
    
        if(MPIconf.is_first_line()){   
            MPI_File_write_at(fZondLine, 0, &info, 1, MPI_FLOAT, &status);
        }
    }

    for( auto j = 0; j < numCells; ++j){
        floatData[6 * j    ] = float(fieldE(indz,j).z() );
        floatData[6 * j + 1] = float(fieldE(indz,j).r() );
        floatData[6 * j + 2] = float(fieldE(indz,j).p() );
        floatData[6 * j + 3] = float(fieldB(indz,j).z() );
        floatData[6 * j + 4] = float(fieldB(indz,j).r() );
        floatData[6 * j + 5] = float(fieldB(indz,j).p() );
    }
  
    int startWrite = MPIconf.rank_line()*sizeData*sizeof(float) + sizeof(float);
  
    int sizeWrite = MPIconf.size_line()*sizeData*sizeof(float);
    
    startWrite += sizeWrite*currentStep;

    MPI_File_write_at(fZondLine, startWrite ,floatData, sizeData, MPI_FLOAT, &status);
    currentStep++;
   // delete[] floatData;
}

void Writer::diag_zond_lineZ_bin(const Array2D<double3>& fieldE, const Array2D<double3>& fieldB, long timestep){
  auto numZonds = zondLineZ[0];
  if (! _world.MPIconf.is_master_depth() ||  numZonds <= 0) return;
  long indz;
  static MPI_File fZondLine[5];
  static long currentStep[5] = {0,0,0,0,0}; 

  assert (numZonds < 5 && "Number of files less then number of zonds!\n");
   
  for (auto n = 0; n < numZonds; ++n ){
    if( ! _world.region.in_region(zondLineZ[n+1]) ) continue;
    indz = long( (zondLineZ[n+1] -  _world.region.origin) / Dz);
    write_zond_lineZ_bin(_mesh.fieldE, _mesh.fieldB, indz, _world.MPIconf, _world.region, fZondLine[n], currentStep[n],timestep);
    currentStep[n]++;
  }
  
}

/*
void DiagZondLineZ(const Array2D<double3>& fieldE, const Array2D<double3>& fieldB, const MPI_Topology& MPIconf, const Region& domain, long timestep){
  
  if (!MPIconf.is_master_depth()) return;
    std::stringstream ss;
  char filename[100];
  static FILE *fZondLine; 
  long skip = 1;
  
  if( rZondLine < 0) return;
    
  long j = rZondLine / Dr;
 
  sprintf(filename, "ZondLineTop%03d",MPIconf.rank_line());

  if( timestep == StartTimeStep){
    fZondLine = fopen(filename, "w");
    //for( long i = domain.dampCells_d1[0]; i < domain.numCells_d1 - domain.dampCells_d1[1] - ADD_NODES; i += skip){
    //  ss << Dx*i + domain.origin << " ";
   // ss << "\n";
  }

  for( long i = domain.dampCells_d1[0]; i < domain.numCells_d1 - domain.dampCells_d1[1]; i += skip){
    ss << float(fieldE(i,j).z() ) << 
    //" " << float(fieldE(i,j).r) << " " << float(fieldE(i,j).p) <<    
    //" " << float(fieldB(i,j).z) << " " float(fieldB(i,j).r) << 
    " " << float(fieldB(i,j).p() ) << " ";
  }
  ss << "\n";
  fprintf(fZondLine, "%s",  ( ss.str() ).c_str() );

}*/
/*
void RWriter::DiagZondLineRbin(const Array2D<double3>& fieldE, const Array2D<double3>& fieldB, const MPI_Topology& MPIconf, const Region& domain, long timestep){

  
  static FILE *fZondLineRbin; 
  long skip = 1;
  float temp;

  if (zZondLine < 0) return;
  if( ! domain.in_region(zZondLine) ) return;

  long i = round( (zZondLine - domain.origin) / Dz);
  

  
  if( timestep == StartTimeStep){
    fZondLineRbin = fopen("ZondLineLeft.bin", "wb");
    temp = float( domain.numCells_d2 / skip);   
    fwrite(&temp, sizeof(float), 1, fZondLineRbin);
  }
   
  for( long j = 0; j < domain.numCells_d2; j += skip){
    temp = float(fieldE(i,j).z() );    
    fwrite(&temp, sizeof(float), 1, fZondLineRbin);
    temp = float(fieldE(i,j).r() );    
    fwrite(&temp, sizeof(float), 1, fZondLineRbin);
    temp = float(fieldE(i,j).p() );    
    fwrite(&temp, sizeof(float), 1, fZondLineRbin);
    temp = float(fieldB(i,j).z() );    
    fwrite(&temp, sizeof(float), 1, fZondLineRbin);
    temp = float(fieldB(i,j).r() );    
    fwrite(&temp, sizeof(float), 1, fZondLineRbin);
    temp = float(fieldB(i,j).p() );    
    fwrite(&temp, sizeof(float), 1, fZondLineRbin);
  }
  
}

void DiagZondLineR2bin(const Array2D<double3>& fieldE, const Array2D<double3>& fieldB, const MPI_Topology& MPIconf, const Region& domain, long timestep){

  
  static FILE *fZondLineRbin; 
  long skip = 1;
  float temp;

  if (zZondLine2 < 0) return;
  if( ! domain.in_region(zZondLine2) ) return;

  long i = round( (zZondLine2 - domain.origin) / Dz);
  
  
  if( timestep == StartTimeStep){
    fZondLineRbin = fopen("ZondLineRight.bin", "wb");
    temp = float( domain.numCells_d2 / skip);   
    fwrite(&temp, sizeof(float), 1, fZondLineRbin);
  }
   
  for( long j = 0; j < domain.numCells_d2; j += skip){
    temp = float(fieldE(i,j).z() );    
    fwrite(&temp, sizeof(float), 1, fZondLineRbin);
    temp = float(fieldE(i,j).r() );    
    fwrite(&temp, sizeof(float), 1, fZondLineRbin);
    temp = float(fieldE(i,j).p() );    
    fwrite(&temp, sizeof(float), 1, fZondLineRbin);
    temp = float(fieldB(i,j).z() );    
    fwrite(&temp, sizeof(float), 1, fZondLineRbin);
    temp = float(fieldB(i,j).r() );    
    fwrite(&temp, sizeof(float), 1, fZondLineRbin);
    temp = float(fieldB(i,j).p() );    
    fwrite(&temp, sizeof(float), 1, fZondLineRbin);
  }
  
}
*/

void Writer::write_fields2D(const Array2D<double3>& fieldE, const Array2D<double3>& fieldB,  const long& timestep){
    if (!_world.MPIconf.is_master_depth()) return;
    
    MPI_File fField2D;
    MPI_Status status;
    
    char filename[50];
    float info;
    long size_z = fieldE.size_d1();
    long size_r = fieldE.size_d2();
    int sizeData = (size_z - (2 * SHAPE_SIZE - 1))*size_r;
    float* floatData[6];

    for(auto i = 0; i<6; i++){
        floatData[i] = new float[size_z*size_r];
    }

    sprintf(filename, ".//Fields//Diag2D//Field2D%03ld",timestep / TimeStepDelayDiag2D);

    MPI_File_open(_world.MPIconf.comm_line(), filename, MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &fField2D);

    for( auto i = 0; i < size_z; i++ ){
      for( auto j = 0; j < size_r; j++ ){
          floatData[0][i*size_r + j] = float(fieldE(i,j).z() );
          floatData[1][i*size_r + j] = float(fieldE(i,j).r() );
          floatData[2][i*size_r + j] = float(fieldE(i,j).p() );
          floatData[3][i*size_r + j] = float(fieldB(i,j).z() );
          floatData[4][i*size_r + j] = float(fieldB(i,j).r() );
          floatData[5][i*size_r + j] = float(fieldB(i,j).p() );
      }
    }
    
    if(_world.MPIconf.is_first_line()){
        info = float(PlasmaCellsZ_glob + DampCellsZ_glob[0] + DampCellsZ_glob[1]);
        MPI_File_write_at(fField2D, 0, &info, 1, MPI_FLOAT, &status);
        info = float(size_r);
        MPI_File_write_at(fField2D, sizeof(float), &info, 1, MPI_FLOAT, &status);
    }
    
    int startWrite = _world.MPIconf.rank_line()*(PlasmaCellsZ_glob/_world.MPIconf.size_line())*size_r*sizeof(float) + 2*sizeof(float);
    
    if(!_world.MPIconf.is_first_line() ){ 
      startWrite += DampCellsZ_glob[0] * size_r * sizeof(float);
    }
    int sizeWrite = (PlasmaCellsZ_glob + DampCellsZ_glob[0] + DampCellsZ_glob[1])*size_r*sizeof(float);
    
    for(auto i = 0; i<6; ++i){
        MPI_File_write_at(fField2D, startWrite + sizeWrite*i,floatData[i], sizeData, MPI_FLOAT, &status);
    }
    
    MPI_File_close(&fField2D);
    
    for(auto i = 0; i<6; i++){
        delete[] floatData[i];
    }

}