#include "Diagnostic.h"


void Writer::write_particles2D(long timestep){ 
  char filename[100];

    for( const auto& sp : _species){

      //sp.density_on_grid_update();

      sprintf(filename, (".//Particles//"+sp.name+"//Diag2D//Dens2D"+"%03ld").c_str(),timestep / TimeStepDelayDiag2D);
      write_array2D(sp.densityOnGrid, sp.densityOnGrid.size_d1() - ADD_NODES, sp.densityOnGrid.size_d2(), filename, _world.MPIconf);
      //sp.phase_on_grid_update();

      sprintf(filename, (".//Particles//"+sp.name+"//Diag2D//Phase2D"+"%03ld").c_str(),timestep / TimeStepDelayDiag2D);
      write_array2D(sp.phaseOnGrid, sp.phaseOnGrid.size_d1() - ADD_NODES, sp.phaseOnGrid.size_d2(),filename, _world.MPIconf);

    }
  
}
