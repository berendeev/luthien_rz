#include "Diagnostic.h"

void DiagData::calc_energy(const Mesh &mesh, const std::vector<ParticlesArray> &species){
  
  for ( auto &sp : species){
    energyParticlesKinetic[sp.name] = sp.get_kinetic_energy();
    energyParticlesInjectPrev[sp.name] = energyParticlesInject[sp.name];
    energyParticlesInject[sp.name] = sp.get_inject_energy();
  }

  energyFieldE = mesh.get_fieldE_energy();
  energyFieldB = mesh.get_fieldB_energy() - mesh.get_fieldB0_energy();

}

void Writer::write_energies(long timestep){
  std::stringstream ss;

  const MPI_Topology &MPIconf = _world.MPIconf;

  if(timestep == 0){
    ss << "Time ";
    for (auto it = diagData.energyParticlesKinetic.begin(); it != diagData.energyParticlesKinetic.end(); ++it){
      ss << "Area_" << it->first << " ";
    }
    for (auto it = diagData.energyParticlesInject.begin(); it != diagData.energyParticlesInject.end(); ++it){
      ss << "Injection_" << it->first << " ";
    }
    ss << "Area_E^2 " << "Area_B^2 " << "powerRadLeft "<< "powerRadRight "<< "powerRadTop " 
                      << "powerRadAvgLeft "<< "powerRadAvgRight "<< "powerRadAvgTop\n";
  }
  
  ss << timestep*Dt << " ";
    
  for (auto it = diagData.energyParticlesKinetic.begin(); it != diagData.energyParticlesKinetic.end(); ++it){
    double energyParticles = it->second;
    MPI_Allreduce ( MPI_IN_PLACE, &energyParticles, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD );
    ss << energyParticles << " ";
  }
  
  for (auto it = diagData.energyParticlesInject.begin(); it != diagData.energyParticlesInject.end(); ++it){
    double energyInject = it->second - diagData.energyParticlesInjectPrev[it->first];
    MPI_Allreduce ( MPI_IN_PLACE, &energyInject, 1, MPI_DOUBLE, MPI_SUM, MPIconf.comm_line() );
    ss << energyInject << " ";
  }
  std::vector<double> vecEnergy;
  vecEnergy.push_back(diagData.energyFieldE);
  vecEnergy.push_back(diagData.energyFieldB);
  vecEnergy.push_back(diagData.powerRad.left);
  vecEnergy.push_back(diagData.powerRad.right);
  vecEnergy.push_back(diagData.powerRad.top);
  vecEnergy.push_back(diagData.powerRadAvg.left);
  vecEnergy.push_back(diagData.powerRadAvg.right);
  vecEnergy.push_back(diagData.powerRadAvg.top);
  
  MPI_Allreduce ( MPI_IN_PLACE, &vecEnergy[0], vecEnergy.size(), MPI_DOUBLE, MPI_SUM, MPIconf.comm_line() );

  for(uint i = 0; i< vecEnergy.size(); ++i){
  ss << vecEnergy[i] << " "; 
  }
  ss <<"\n";
  if( MPIconf.is_master() ){
      fprintf(fDiagEnergies, "%s",  ( ss.str() ).c_str() ); 
      std::cout << ss.str();
  }
    if( MPIconf.is_master() && timestep % TimeStepDelayDiag1D == 0){
      fflush(fDiagEnergies);
    }


}

void DiagData::calc_radiation_Pointing(const Mesh &mesh,const Region &region){
  long i,j;
  double3 fieldEAvg;
  double3 fieldBAvg;
  
  powerRad.clear();
  
  if(region.boundType_d1[0] == BOUND_TYPE_OPEN){
      i = (zondRadZ[0] - region.origin) / Dz;
      for( j = 0; j < region.numCells_d2; j++){
          fieldEAvg = mesh.get_fieldE_in_cell(i,j);
          fieldBAvg = mesh.get_fieldB_in_cell(i,j);
          powerRadLine.left(j) += Dt*(2.0*PI*(j+0.5)*Dr*Dr)*fabs(fieldEAvg.r() * fieldBAvg.p() 
                                                          - fieldEAvg.p() * fieldBAvg.r() );
          powerRad.left += powerRadLine.left(j);
        }
    }
    if(region.boundType_d1[1] == BOUND_TYPE_OPEN){
        i = (zondRadZ[1] - region.origin) / Dz;
        for( j = 0; j < region.numCells_d2 ; j++){
          fieldEAvg = mesh.get_fieldE_in_cell(i,j);
          fieldBAvg = mesh.get_fieldB_in_cell(i,j);
          powerRadLine.right(j) += Dt*(2.0*PI*(j+0.5)*Dr*Dr)*fabs(fieldEAvg.r() * fieldBAvg.p() 
                                                          - fieldEAvg.p() * fieldBAvg.r() );
          powerRad.right += powerRadLine.right(j);
        }
    }
      j = zondRadR / Dr;
    for (i = 0; i < region.numCells_d1 ; i++){
        fieldEAvg = mesh.get_fieldE_in_cell(i,j);
        fieldBAvg = mesh.get_fieldB_in_cell(i,j);
        powerRadLine.top(i) += Dt*(2.0*PI*j*Dr*Dz)*fabs(fieldEAvg.p()*fieldBAvg.z()
                                                   -fieldEAvg.z()*fieldBAvg.p());
        powerRad.top += powerRadLine.top(i);
    }
    
}

void DiagData::calc_radiation_Pointing_avg(const Mesh &mesh,const Region &region,long timestep){
  long i,j,t;
  long sizeT = long(2 * PI / Dt);
  long t1 = timestep % sizeT;
  double3 fieldE;
  double3 fieldB;
  
  if(region.boundType_d1[0] == BOUND_TYPE_OPEN){
      i = (zondRadZ[0] - region.origin) / Dz;
      for( j = 0; j < region.numCells_d2; j++){
          fieldE = fieldB = 0.;
          for( t =  0; t < fieldEAvgLine.left.size_d2(); t++){
            fieldE += fieldEAvgLine.left(j,t);
            fieldB += fieldBAvgLine.left(j,t);
          }

          fieldE = mesh.get_fieldE_in_cell(i,j)
                              - Dt/(2.*PI)*fieldE;
          fieldB = mesh.get_fieldB_in_cell(i,j)
                              - Dt/(2.*PI)*fieldB;

          powerRadAvgLine.left(j,t1) = Dt*2.*PI*(j+0.5)*Dr*Dr*fabs(fieldE.r() * fieldB.p() 
                                                  - fieldE.p() * fieldB.r() );
        }
    }
    if(region.boundType_d1[1] == BOUND_TYPE_OPEN){
      i = (zondRadZ[1]-region.origin) / Dz;
      for( j = 0; j < region.numCells_d2 ; j++){
          fieldE = fieldB = 0.;
          for( t =  0; t < fieldEAvgLine.right.size_d2(); t++){
            fieldE += fieldEAvgLine.left(j,t);
            fieldB += fieldBAvgLine.left(j,t);
          }

          fieldE = mesh.get_fieldE_in_cell(i,j) 
                            - Dt/(2.*PI)*fieldE;

          fieldB = mesh.get_fieldB_in_cell(i,j)
                             - Dt/(2.*PI)*fieldB;

          powerRadAvgLine.right(j,t1) = Dt*2.*PI*(j+0.5)*Dr*Dr*fabs(fieldE.r() * fieldB.p() 
                                                  - fieldE.p() * fieldB.r() );

        }
    }
    j = zondRadR / Dr;
    for ( i = 0; i < region.numCells_d1; i++){
         fieldE =  fieldB = 0.;
        for( t =  0; t < fieldEAvgLine.top.size_d2(); t++){
            fieldE += fieldEAvgLine.top(i,t);
            fieldB += fieldBAvgLine.top(i,t);
        }
        fieldE = mesh.get_fieldE_in_cell(i,j)
                          - Dt/(2.*PI)*fieldE;
        fieldB = mesh.get_fieldB_in_cell(i,j)
                          - Dt/(2.*PI)*fieldB;
        powerRadAvgLine.top(i,t1) = Dt*(2.0*PI*j*Dr*Dz)*fabs(fieldE.p() * fieldB.z()
                                                         -fieldE.z() * fieldB.p() );
    }
    
}
void Writer::write_radiation_line(long timestep){
    const Region &region = _world.region;
    const MPI_Topology &MPIconf = _world.MPIconf;

    if (!MPIconf.is_master_depth()) return;

    static MPI_File fRadTop,fRadLeft,fRadRight;
    MPI_Status status;
    char filename[50];
    int sizeDataZ = (region.numCells_d1 - region.dampCells_d1[0] - region.dampCells_d1[1]);
    int sizeDataR = (region.numCells_d2 - region.dampCells_d2);
    static float* floatDataZ = new float[sizeDataZ];
    static float* floatDataR = new float[sizeDataR];
    static long currentStep = 0; 
    int startWrite, sizeWrite;
    float info;
    long i,indx; 

    if( timestep == StartTimeStep){
        sprintf(filename, ".//Fields//Diag1D//PRadTop.bin" );
        MPI_File_open(MPIconf.comm_line(), filename, MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &fRadTop);
        info = float(sizeDataZ*MPIconf.size_line());
        if(MPIconf.is_first_line()){   
            MPI_File_write_at(fRadTop, 0, &info, 1, MPI_FLOAT, &status);
        }

        sprintf(filename, ".//Fields//Diag1D//PRadLeft.bin" );
        MPI_File_open(MPIconf.comm_line(), filename, MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &fRadLeft);
        info = float(sizeDataR);
        if(MPIconf.is_first_line()){   
            MPI_File_write_at(fRadLeft, 0, &info, 1, MPI_FLOAT, &status);
        }

        sprintf(filename, ".//Fields//Diag1D//PRadRight.bin" );
        MPI_File_open(MPIconf.comm_line(), filename, MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &fRadRight);
        info = float(sizeDataR);
        if(MPIconf.is_first_line()){   
            MPI_File_write_at(fRadRight, 0, &info, 1, MPI_FLOAT, &status);
        }
      MPI_Barrier(MPIconf.comm_line());
    }
    indx = region.dampCells_d1[0];
    for( i = 0; i < sizeDataZ; i++){
      floatDataZ[i] = float(diagData.powerRadLine.top(i+indx) );
    }
    startWrite = MPIconf.rank_line()*sizeDataZ*sizeof(float) + sizeof(float);
    sizeWrite = MPIconf.size_line()*sizeDataZ*sizeof(float);
    startWrite += sizeWrite*currentStep;

    MPI_File_write_at(fRadTop, startWrite, floatDataZ, sizeDataZ, MPI_FLOAT, &status);

    startWrite = sizeDataR*sizeof(float)*currentStep;
    
    if(region.boundType_d1[0]==BOUND_TYPE_OPEN){
        for(i = 0; i < sizeDataR; i++){
            floatDataR[i] = float(diagData.powerRadLine.left(i) );
        }
        MPI_File_write_at(fRadLeft, startWrite + sizeof(float), floatDataR, sizeDataR, MPI_FLOAT, &status);
    }

    if(region.boundType_d1[1] == BOUND_TYPE_OPEN){
        for(i = 0; i < sizeDataR; i++){
            floatDataR[i] = float(diagData.powerRadLine.right(i) );
        }
        MPI_File_write_at(fRadRight, startWrite + sizeof(float), floatDataR, sizeDataR, MPI_FLOAT, &status);
    }

    currentStep++;
}

void Writer::write_radiation_avg_line(long timestep){
    const Region &region = _world.region;
    const MPI_Topology &MPIconf = _world.MPIconf;

    if (!MPIconf.is_master_depth()) return;

    static MPI_File fRadTop,fRadLeft,fRadRight;
        MPI_Status status;

    char filename[50];
    int sizeDataZ = (region.numCells_d1 - region.dampCells_d1[0] - region.dampCells_d1[1]);
    int sizeDataR = (region.numCells_d2 - region.dampCells_d2);
    static float* floatDataZ = new float[sizeDataZ];
    static float* floatDataR = new float[sizeDataR];
    static long currentStep = 0; 
    int startWrite, sizeWrite;
    float info;
    double pRad; 
    long i,indx; 

    if( timestep == StartTimeStep){
        sprintf(filename, ".//Fields//Diag1D//PRadAvgTop.bin" );
        MPI_File_open(MPIconf.comm_line(), filename, MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &fRadTop);
        info = float(sizeDataZ*MPIconf.size_line());
        if(MPIconf.is_first_line()){   
            MPI_File_write_at(fRadTop, 0, &info, 1, MPI_FLOAT, &status);
        }

        sprintf(filename, ".//Fields//Diag1D//PRadAvgLeft.bin" );
        MPI_File_open(MPIconf.comm_line(), filename, MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &fRadLeft);
        info = float(sizeDataR);
        if(MPIconf.is_first_line()){   
            MPI_File_write_at(fRadLeft, 0, &info, 1, MPI_FLOAT, &status);
        }

        sprintf(filename, ".//Fields//Diag1D//PRadAvgRight.bin" );
        MPI_File_open(MPIconf.comm_line(), filename, MPI_MODE_CREATE|MPI_MODE_WRONLY, MPI_INFO_NULL, &fRadRight);
        info = float(sizeDataR);
        if(MPIconf.is_first_line()){   
            MPI_File_write_at(fRadRight, 0, &info, 1, MPI_FLOAT, &status);
        }
    }
    indx = region.dampCells_d1[0];
    for( i = 0; i < sizeDataZ; i++){
      pRad = diagData.powerRadAvgLine.top.sum_d2(i+indx)/(2*PI);
      floatDataZ[i] = float( pRad);
      diagData.powerRadAvg.top += pRad;
    }
    startWrite = MPIconf.rank_line()*sizeDataZ*sizeof(float) + sizeof(float);
    sizeWrite = MPIconf.size_line()*sizeDataZ*sizeof(float);
    startWrite += sizeWrite*currentStep;
    MPI_File_write_at(fRadTop, startWrite, floatDataZ, sizeDataZ, MPI_FLOAT, &status);

    startWrite = sizeDataR*sizeof(float)*currentStep;
    
    if(region.boundType_d1[0]==BOUND_TYPE_OPEN){
        for(i = 0; i < sizeDataR; i++){
            pRad = diagData.powerRadAvgLine.left.sum_d2(i)/(2*PI);
            floatDataR[i] = float( pRad);
            diagData.powerRadAvg.left += pRad;        
        }
        MPI_File_write_at(fRadLeft, startWrite + sizeof(float), floatDataR, sizeDataR, MPI_FLOAT, &status);
    }

    if(region.boundType_d1[1]==BOUND_TYPE_OPEN){
        for(i = 0; i < sizeDataR; i++){
            pRad = diagData.powerRadAvgLine.right.sum_d2(i)/(2*PI);
            floatDataR[i] = float( pRad);
            diagData.powerRadAvg.right += pRad;  
        }
        MPI_File_write_at(fRadRight, startWrite + sizeof(float), floatDataR, sizeDataR, MPI_FLOAT, &status);
    }

    currentStep++;
}

/*
void RWriter::write_radiation_avg1D(long timestep){
  const Region &region = _world.region;
  const MPI_Topology &MPIconf = _world.MPIconf;
  
  if (!MPIconf.is_master_depth()) return;
    
  FILE* fDamp;
  char filename[100];
  long i,t;
  double pRad;

  diagData.powerRadAvg.clear();

  sprintf(filename, ".//Fields//Diag1D//PRadAvgTop_%03ld_%03d",timestep / TimeDelay1D,MPIconf.rank_line() );
  fDamp = fopen(filename, "w");
  fprintf(fDamp, "%s %s %f\n",  "# z_coord", "Pointing_Radiation r =",fieldsAvgLine_r); 
  for( i = region.dampCells_d1[0]; i < region.numCells_d1 - region.dampCells_d1[1] ;i++){
    pRad = 0;
    for( t =  0; t < diagData.powerRadAvgLine.top.size_d2(); t++){
      pRad += diagData.powerRadAvgLine.top(i,t)/ (2.*PI);
    }

    fprintf(fDamp, "%g %g\n",  Dz*i + region.origin - Dz*DampCellsZ_glob[0], pRad);
    diagData.powerRadAvg.top += pRad;
  }
  fclose(fDamp);
  
  if(region.boundType_d1[0]==OPEN){
      sprintf(filename, ".//Fields//Diag1D//PRadAvgLeft_%03ld",timestep / TimeDelay1D);
      fDamp = fopen(filename, "w"); 
      fprintf(fDamp, "%s %s %f\n",  "# r_coord", "Pointing_Radiation, z =", fieldsAvgLine_z1 - Dz*DampCellsZ_glob[0]); 
      for(i = 0; i < region.numCells_d2 - region.dampCells_d2;i++){
        pRad = 0;
        for( t =  0; t < diagData.powerRadAvgLine.left.size_d2(); t++){
          pRad += diagData.powerRadAvgLine.left(i,t)/ (2.*PI);
        }
        fprintf(fDamp, "%g %g\n",  Dr*i, pRad);
        diagData.powerRadAvg.left += pRad;
      }
      fclose(fDamp);
  }
  
  if(region.boundType_d1[1]==OPEN){
      sprintf(filename, ".//Fields//Diag1D//PRadAvgRight_%03ld",timestep / TimeDelay1D);
      fDamp = fopen(filename, "w"); 
      fprintf(fDamp, "%s %s %f\n",  "# r_coord", "Pointing_Radiation, z =", fieldsAvgLine_z2 - Dz*DampCellsZ_glob[0]); 
      for(i = 0; i < region.numCells_d2 - region.dampCells_d2;i++){
        pRad = 0;
        for( t =  0; t < diagData.powerRadAvgLine.right.size_d2(); t++){
          pRad += diagData.powerRadAvgLine.right(i,t) / (2.*PI);
        }
        fprintf(fDamp, "%g %g\n",  Dr*i, pRad);
        diagData.powerRadAvg.right += pRad;
      }
      fclose(fDamp);    
  }
}
*/


