#ifndef LASER_H_
#define LASER_H_
#include "World.h"
#include "Read.h"

struct Laser{
    double tau;
    double w0;
    double vg;
    double focus_z;
    double start_z;
    double sigma0;
    double a0;
    double delay;
    
    Laser(const std::vector<std::string>& vecStringParams);
    void set_params_from_string(const std::string& line);

    double3 force(double2 x, long timestep) const;
    double get_field_coll(double2 x, long timestep) const;
};

#endif 
