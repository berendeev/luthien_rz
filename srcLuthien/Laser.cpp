#include "Laser.h"

inline double pow2(double a){
  return a*a;
}
inline double pow3(double a){
  return a*a*a;
}
inline double pow4(double a){
  return a*a*a*a;
}

Laser::Laser(const std::vector<std::string>& vecStringParams){
    for (const auto& line: vecStringParams){
        set_params_from_string(line);
    }

}

// Устанавливаем значения основных параметров в переменной Params в соответствии с содержимым сроки
void Laser::set_params_from_string(const std::string& line){
    std::vector<std::string> strvec;

    strvec = split(line, ' ');

    if(strvec[0]=="tau"){
        tau = stod(strvec[1]);
    }

    if(strvec[0]=="w0"){
        w0 = stod(strvec[1]);
    }

    if(strvec[0]=="vg"){
       vg =  stod(strvec[1]);
    }
    if(strvec[0]=="sigma0"){
       sigma0 =  stod(strvec[1]);
    }
    if(strvec[0]=="focus_z"){
        focus_z = stod(strvec[1]);
    }
    if(strvec[0]=="start_z"){
        start_z = stod(strvec[1]);
    }
    if(strvec[0]=="a0"){
       a0 = stod(strvec[1]);
    }
    if(strvec[0]=="delay"){
        delay = stod(strvec[1]);
    }
   
}


double3 Laser::force(double2 x, long timestep) const{
   double3 F;
   double z = x.z();
   double r = x.r();
   double startLas;
   double cTime = timestep*Dt;

    startLas = (z - start_z) / vg + delay; 

    if( cTime  >= startLas && cTime  <= startLas + 2 * tau) {
      const double RR = 0.5 * w0 * sigma0 * sigma0;
      const double sigma = sigma0 * sqrt(1. + pow2( (z - focus_z) / RR) );
      const double w = 0.5*PI*(cTime - startLas) / tau;
      const double Psin = sin(w);
      const double Pcos = cos(w);
      F.z() =  0.5 * PI * pow2(a0) / tau / vg * pow2(sigma0) / pow2(sigma) * pow3(Psin) * Pcos * exp(-2.*pow2(r / sigma));
      F.r() = r * pow2(a0) * pow2(sigma0) / pow4(sigma) * pow4(Psin) * exp(-2. * pow2(r / sigma));
  }
   return F;

}

double Laser::get_field_coll(double2 x, long timestep) const{
   double f = 0.;
   double z = x.z();
   double r = x.r();
    double startLas;
   double cTime = timestep*Dt;

    startLas = (z - start_z) / vg + delay; 
  //    std::cout << z << " " <<  start_z << " "<<vg << " " <<delay  << "\n";

    if( cTime >= startLas && cTime <= startLas + 2 * tau) {
      const double RR = 0.5 * w0 * sigma0 * sigma0;
      const double sigma = sigma0 * sqrt(1. + pow2( (z - focus_z) / RR) );
      const double w = 0.5*PI*(cTime - startLas) / tau;

      f =  a0 * w0 * (sigma0 / sigma) * exp( - pow2(r / sigma) ) * pow2(sin(w));
  }
   return f;

}