# Python script for setting parameters
import pprint
import sys
sys.path.insert(0, "./Scripts")
from setInitParams import *

DirName = "Res_Luthien1.0"

EM_Solver = enum("NONE","FDTD")

EM_SOLVER = EM_Solver.FDTD

EM_Bound = enum("DAMP","PML")

EM_BOUND = EM_Bound.PML

BoundType = enum("NONE","PERIODIC","OPEN","NEIGHBOUR")

BoundTypeZ_glob = [BoundType.OPEN, BoundType.OPEN]
BoundTypeR_glob = BoundType.OPEN


Queue = "home" # type of queue
Cluster = "nsu" # cluster name (spb, nsu, sscc)

####
RECOVERY = 0

DEBUG = 1

SHAPE = 2 # 1 - PIC, 2 - Parabolic e.t.c

IonizationType = enum("NONE","FIELD")
IONIZATION_TYPE = IonizationType.NONE # Fields ionization

PARTICLE_MASS = True ### Particle has individual mass

UPD_PARTICLES = True

UPD_FIELDS = True # Update Fields (for Debug)


#####

NumProcs = 4 # number of processors
NumAreas = 4 # Number of decomposition region


Dr = 0.05 # step on X
Dz = 0.05 # step on Y


PlasmaCellsR_glob = 100 # Number of cells for Plasma on R 
PlasmaCellsZ_glob = 1280 # Number of cells for Plasma on Z

NumCellsR_glob = 300 # Number of all cells in computation domain on R


DampCellsZ_glob = [80,80] # Number of Damping layer cells on Z
DampCellsZP_glob = [80,80] # Number of Damping layer cells on Z near Plasma 
DampCellsR_glob = 80 # Number of Damping layer cells on R

NumCellsZ_glob = PlasmaCellsZ_glob + DampCellsZ_glob[0]+DampCellsZ_glob[1] # Number of all cells in computation domain on Z


NumPartPerLine = 2 # Number of particles per line segment cell 
NumPartPerCell = NumPartPerLine * NumPartPerLine # Number of particles per cell

MaxTime = 800 # in 1/w_p
RecTime = 100 #


DiagDelay2D = 4 # in 1 / w_p
DiagDelay1D = 1 # in 1 / w_p
#DiagDelayEnergy1D = 1 

BUniform = [0.3, 0.0, 0.0] # in w_c / w_p

### Magnetic field from coil with current
### Number of coils, [z,r coord, current]
BCoil = [0, 0.5*NumCellsZ_glob*Dz ,100., 20.] 

###### PHYSIC CONST
PI = 3.141592653589793
me = 9.10938356e-28 # electron mass
ee = 4.80320427e-10 # electron charge
n0 = 5.e15 # particles / cm^3
#n0 = 2.5e18 # particles / cm^3
w_p = (4*PI*n0*ee*ee/me)**0.5
cc = 2.99792458e10 # speed on light cm/sec 
MC2 = 511.
########



#######################################


Dt =  0.5*min(Dr,Dz)  # time step

MaxTimeStep = int(round(MaxTime/Dt+1))
RecTimeStep = int(round(RecTime/Dt))
StartTimeStep = int(round(RECOVERY/Dt))
TimeStepDelayDiag2D = int(round(DiagDelay2D/Dt))
TimeStepDelayDiag1D = int(round(DiagDelay1D/Dt))
#EnergyOutputDelay1D = int(round(DiagDelayEnergy1D/Dt))


#### ZONDS ##############
numZonds = 15
### First number - number of zonds
zondZ = [numZonds] + list(PlasmaCellsZ_glob / (numZonds + 1) * Dz * (z+1) + Dz* DampCellsZ_glob[0] for z in range(numZonds)  )
zondR = [numZonds] + [ Dr * (NumCellsR_glob - DampCellsR_glob - 30) ] * numZonds

if len(zondR) != len(zondZ):
  print("Diff numbers of zonds coord\n")
  exit()

### First number - number of zonds
zondLineR = [3,5*Dr, 25*Dr, Dr * (NumCellsR_glob - DampCellsR_glob - 30)]
zondLineZ = [0.,Dz * (DampCellsZ_glob[0] + 10)]

########################################
#### Coords for radiation diagnostic
zondRadR = [Dr * (NumCellsR_glob - DampCellsR_glob - 30)]
zondRadZ = [Dz * (DampCellsZ_glob[0] + 10), Dz * (NumCellsZ_glob - DampCellsZ_glob[1] - 10)]

PzMax = 800 // NumAreas
PpMax = 800

MaxSizeOfParts=4*NumPartPerCell*PlasmaCellsZ_glob*PlasmaCellsR_glob/NumProcs+1


NumOfPartSpecies = 0

PartParams = {} # 
 # 
isVelDiag = 0


PName="Neutrals"

Exist = False
PartDict = {}
PartDict["Charge"] = 0.0
PartDict["Density"] = 0.5
PartDict["Velocity"] = 0.0
PartDict["Mass"] = 4.*1836.0
Pot_I = 0.02459 # Kev
PartDict["Pot_I"] = Pot_I
PartDict["Pot_k"] = 1.
PartDict["Temperature"] = 0;# (0.014/512.)**0.5
PartDict["Pz_max"] = 1.0 # 
PartDict["Pz_min"] = -1.0 #
PartDict["Width"] = PlasmaCellsR_glob * Dr
PartDict["Shift"] = 0.0
PartDict["SmoothMass"] = 1
InitDist = "StrictUniform"
dn0 = 0.0
k0=3.55
PartDict["DistParams"] = [str(InitDist), dn0, k0]



if Exist :
    NumOfPartSpecies+=1
    setParams(PartParams, PName, PartDict)


PName="Electrons"

Exist = True
PartDict = {}
PartDict["Charge"] = -1.0
PartDict["Density"] = 1.0
PartDict["Velocity"] = 0.0
PartDict["Mass"] = 1.0
Temperature = 0.04 #  KeV
PartDict["Temperature"] = (Temperature/512.)**0.5
PartDict["Pz_max"] = 1.e-1 # 
PartDict["Pz_min"] = -1.e-1 #
PartDict["Width"] = PlasmaCellsR_glob * Dr
PartDict["Shift"] = 0.0
PartDict["SmoothMass"] = 0.0
PartDict["BoundResumption"] = 1
InitDist = "None"
InitDist = "StrictUniform"
#InitDist = "UnderForceLine_Z_R"
dn0 = 0.0
k0=3.55
PartDict["DistParams"] = [str(InitDist), dn0, k0]
PartDict["DistParams"] = [str(InitDist), DampCellsZ_glob[0]*Dz, 3.]


if Exist:
    NumOfPartSpecies+=1
    setParams(PartParams, PName, PartDict)

PName="Ions"

Exist = False
PartDict = {}
PartDict["Charge"] = 1.0
PartDict["Density"] = 0.5 #1.0
PartDict["Velocity"] = 0.0
PartDict["Mass"] = 1836.0 * 4
PartDict["Temperature"] = 0.0
PartDict["Pz_max"] = 1.0 # 
PartDict["Pz_min"] = -1.0 #
PartDict["Width"] = PlasmaCellsR_glob * Dr
PartDict["Shift"] = 0.0
PartDict["SmoothMass"] = 0.0
PartDict["SmoothMassSize"] = 0.15*PlasmaCellsZ_glob * Dz
PartDict["SmoothMassMax"] = 50.0
PartDict["BoundResumption"] = 1

InitDist = "StrictUniform"
InitDist = "None"
dn0 = 0.0
k0=3.55
PartDict["DistParams"] = [str(InitDist), dn0, k0]
Pot_I = 0.05442 # Kev
PartDict["Pot_I"] = Pot_I
PartDict["Pot_k"] = 2.

#SmoothMassMax = 800.
#SmoothMassSize = 0.15*PlasmaCellsZ_glob * Dz

if Exist :
    NumOfPartSpecies+=1
    setParams(PartParams, PName, PartDict)


PName="Ions2"

Exist = False
PartDict = {}
PartDict["Charge"] = 2.0
PartDict["Density"] = 0.5
PartDict["Velocity"] = 0.0
PartDict["Mass"] = 4.*1836.0
PartDict["Temperature"] = 0.0
PartDict["SmoothMass"] = 0.0
PartDict["Pz_max"] = 1.0 # 
PartDict["Pz_min"] = -1.0 #
PartDict["Width"] = PlasmaCellsR_glob*Dr
PartDict["Shift"] = 0.0
PartDict["BoundResumption"] = 1

InitDist = "None" #"StrictUniform"
dn0 = 0.0
k0=3.55
PartDict["DistParams"] = [str(InitDist), dn0, k0]

SmoothMassMax = 100.
SmoothMassSize = 10. #0.15*PlasmaCellsZ_glob*Dz

if Exist :
    NumOfPartSpecies+=1
    setParams(PartParams, PName, PartDict)



###### GLOBAL BEAMS PARAMETERS #####################
InjType = 0 # 0 - random inject, 1 - periodic strit inject
InjectSmoothTime = 0.001

Vb = 0.9#79 #0.995

SourceType = enum("NONE","FOCUSED_GAUSS","UNIFORM","UNIFORM_MPW","FORCE_LINE")
#### FOCUSED BEAMS 

Lmax = 10. / (cc / w_p) # cm / c/w_p
Larea = 0.5*PlasmaCellsZ_glob*Dz
Rmax = 2.5 / (cc / w_p) # cm / c/w_p
Rfocus = 0.05  / (cc / w_p) # cm / c/w_p
Zfocus = 0.5 * Dz * PlasmaCellsZ_glob



PName="BeamLeft"

Exist = True
PartDict = {}
PartDict["Charge"] = -1.0
PartDict["Density"] = 0.002
PartDict["Velocity"] = Vb
PartDict["Mass"] = 1.0
Temperature = 4. # KeV
PartDict["Temperature"] = (Temperature/512.)**0.5
PartDict["Pz_max"] = 6.0
PartDict["Pz_min"] = 0.0
PartDict["Width"] = 0.5*Dr * PlasmaCellsR_glob #Rfocus * 0.5
PartDict["Focus"] = 0.5 * Dz * PlasmaCellsZ_glob
PartDict["SourceType"] = SourceType.UNIFORM #FORCE_LINE
PartDict["Shift"] = 0.0
InitDist = "None"
PartDict["DistParams"] = [str(InitDist)]


if Exist :
    NumOfPartSpecies+=1
    setParams(PartParams, PName, PartDict)

PName="BeamRight"

Exist = True
PartDict = {}
PartDict["Charge"] = -1.0
PartDict["Density"] = 0.002
PartDict["Velocity"] = -Vb
PartDict["Mass"] = 1.0
Temperature = 4. # KeV
PartDict["Temperature"] = (Temperature/512.)**0.5
PartDict["Pz_max"] = 0.0
PartDict["Pz_min"] = -6.0
PartDict["Width"] = Dr * PlasmaCellsR_glob
PartDict["Focus"] = 0.5 * Dz * PlasmaCellsZ_glob
PartDict["SourceType"] = SourceType.UNIFORM #FOCUSED_GAUSS
PartDict["Shift"] = 0.0
InitDist = "None"
PartDict["DistParams"] = [str(InitDist)]

if Exist :
    NumOfPartSpecies+=1
    setParams(PartParams, PName, PartDict)
    

### GLOBAL LASERS PARAMETERS ##############################
Las_tau = 3.48
Las_w0 = 25.44
Las_vg = (1. - 1. / (Las_w0**2) )**0.5

LasParams = {} # 

LasName="LaserLeft"
Exist = False
LasDict = {} #
LasDict["delay"] = 0 #2*Las_tau
LasDict["tau"] = Las_tau
LasDict["vg"] = Las_vg
LasDict["a0"] = 0.67
LasDict["sigma0"] = 1.86
LasDict["w0"] = Las_w0
LasDict["focus_z"] =  0.5 * Dz * NumCellsZ_glob
LasDict["start_z"] = Dz*DampCellsZ_glob[0]

if Exist:
    setParams(LasParams, LasName, LasDict)

LasName="LaserRight"
Exist = False
LasDict = {} #
LasDict["delay"] = 0.0
LasDict["tau"] = Las_tau
LasDict["vg"] = -Las_vg
LasDict["a0"] = 0.8
LasDict["sigma0"] =  5.28
LasDict["w0"] = Las_w0
LasDict["focus_z"] =  0.5 * Dz * NumCellsZ_glob
LasDict["start_z"] = Dz*PlasmaCellsZ_glob + Dz*DampCellsZ_glob[0]

if Exist:
    setParams(LasParams, LasName, LasDict)



#####//////////////////////////////

WorkDir = DirName+"_nz_"+str(PlasmaCellsZ_glob)+"_np_"+str(NumPartPerCell )+"_Dz_"+str(Dz)+"_Vb_"+str(Vb)+"_S"+str(SHAPE)






if SHAPE < 3:
    SHAPE_SIZE = 2
    CELLS_SHIFT = 1
else:
    SHAPE_SIZE = 3
    CELLS_SHIFT = 2    

ADD_NODES = 2 * CELLS_SHIFT + 1


if PlasmaCellsZ_glob % NumAreas != 0:
	print("***********************************************")
	print("WARNING!!! Domain decomposition is not correct!!")
	print("***********************************************")

###////////////////////////////////

DefineParams = []
SysParams = []

setConst(SysParams,'const long','NumProcs',[NumProcs],None)
setConst(SysParams,'const long','NumAreas',[NumAreas],None)

setConst(SysParams,'const double','Dr',[Dr],None)
setConst(SysParams,'const double','Dz',[Dz],None)
setConst(SysParams,'const double','Dt',[Dt],None)

setConst(SysParams,'const long','NumCellsZ_glob',[NumCellsZ_glob],None)
setConst(SysParams,'const long','NumCellsR_glob',[NumCellsR_glob],None)
setConst(SysParams,'const long','NumCellsZmax_glob',[NumCellsZ_glob + 2 * SHAPE_SIZE - 1],None)
setConst(SysParams,'const long','NumCellsRmax_glob',[NumCellsR_glob + 2 * SHAPE_SIZE - 1],None)
setConst(SysParams,'const long','DampCellsZ_glob',DampCellsZ_glob,None)
setConst(SysParams,'const long','DampCellsZP_glob',DampCellsZP_glob,None)
setConst(SysParams,'const long','DampCellsR_glob',[DampCellsR_glob],None)
setConst(SysParams,'const long','PlasmaCellsR_glob',[PlasmaCellsR_glob],None)
setConst(SysParams,'const long','PlasmaCellsZ_glob',[PlasmaCellsZ_glob],None)

setConst(SysParams,'const long','NumOfPartSpecies',[NumOfPartSpecies],None)
setConst(SysParams,'const long','NumPartPerLine ',[NumPartPerLine ],None)
setConst(SysParams,'const long','NumPartPerCell',[NumPartPerCell],None)

setConst(SysParams,'const long','MaxTimeStep',[MaxTimeStep],None)
setConst(SysParams,'const long','RecTimeStep',[RecTimeStep],None)
setConst(SysParams,'const long','StartTimeStep',[StartTimeStep],None)
setConst(SysParams,'const long','TimeStepDelayDiag2D',[TimeStepDelayDiag2D],None)
setConst(SysParams,'const long','TimeStepDelayDiag1D',[TimeStepDelayDiag1D],None)
#setConst(SysParams,'const long','EnergyOutputDelay1D',[EnergyOutputDelay1D],None)

setConst(SysParams,'const double','InjectSmoothTime',[InjectSmoothTime],None)

setConst(SysParams,'const double','BUniform',BUniform,None)
setConst(SysParams,'const double','BCoil',BCoil,None)
setConst(SysParams,'const long','BoundTypeR_glob',[BoundTypeR_glob],None)
setConst(SysParams,'const long','BoundTypeZ_glob',BoundTypeZ_glob,None)
#setConst(SysParams,'const double','nZonds',[nZonds],None)
setConst(SysParams,'const double','zondR',zondR,None)
setConst(SysParams,'const double','zondZ',zondZ,None)
setConst(SysParams,'const double','zondLineZ',zondLineZ,None)
setConst(SysParams,'const double','zondLineR',zondLineR,None)
setConst(SysParams,'const double','MC2',[MC2],None)
setConst(SysParams,'const double','n0',[n0],None)

setConst(SysParams,'const double','zondRadR',zondRadR,None)
setConst(SysParams,'const double','zondRadZ',zondRadZ,None)


setConst(SysParams,'const int','isVelDiag',[isVelDiag],None)
setConst(SysParams,'const long','PzMax',[PzMax],None)
setConst(SysParams,'const long','PpMax',[PpMax],None)

setConst(SysParams,'const long','MaxSizeOfParts',[MaxSizeOfParts],None)

setConst(SysParams,'const double','PI',[PI],None)

setConst(SysParams,'const double','Rmax',[Rmax],None)
setConst(SysParams,'const double','Rfocus',[Rfocus],None)
setConst(SysParams,'const double','Lmax',[Lmax],None)
setConst(SysParams,'const double','Larea',[Larea],None)
setConst(SysParams,'const double','SmoothMassMax',[SmoothMassMax],None)
setConst(SysParams,'const double','SmoothMassSize',[SmoothMassSize],None)

if DEBUG == 0:
    setConst(DefineParams,'#define','NDEBUG',[' '],None)
else:
    setConst(DefineParams,'#define','DEBUG',[DEBUG],None)
if PARTICLE_MASS:
	setConst(DefineParams,'#define','PARTICLE_MASS',[1],None)
setConst(DefineParams,'#define','RECOVERY',[RECOVERY],None)
setConst(DefineParams,'#define','SHAPE',[SHAPE],None)
setConst(DefineParams,'#define','SHAPE_SIZE',[SHAPE_SIZE],None)
setConst(DefineParams,'#define','CELLS_SHIFT',[CELLS_SHIFT],None)
setConst(DefineParams,'#define','ADD_NODES',[ADD_NODES],None)
setConst(DefineParams,'#define','IONIZATION_TYPE',[IONIZATION_TYPE],None)
setConst(DefineParams,'#define','IONIZATION_TYPE_FIELD',[IonizationType.FIELD],None)

setConst(DefineParams,'#define','UPD_FIELDS',[int(UPD_FIELDS)],None)
setConst(DefineParams,'#define','UPD_PARTICLES',[int(UPD_PARTICLES)],None)
setConst(DefineParams,'#define','EM_SOLVER',[EM_SOLVER],None)
setConst(DefineParams,'#define','EM_SOLVER_FDTD',[EM_Solver.FDTD],None)

setConst(DefineParams,'#define','EM_BOUND',[EM_BOUND],None)
setConst(DefineParams,'#define','EM_BOUND_PML',[EM_Bound.PML],None)
setConst(DefineParams,'#define','EM_BOUND_DAMP',[EM_Bound.DAMP],None)


setConst(DefineParams,'#define','BOUND_TYPE_PERIODIC',[BoundType.PERIODIC],None)
setConst(DefineParams,'#define','BOUND_TYPE_OPEN',[BoundType.OPEN],None)
setConst(DefineParams,'#define','BOUND_TYPE_NEIGHBOUR',[BoundType.NEIGHBOUR],None)
setConst(DefineParams,'#define','SOURCE_FOCUSED_GAUSS',[SourceType.FOCUSED_GAUSS],None)
setConst(DefineParams,'#define','SOURCE_UNIFORM',[SourceType.UNIFORM],None)
setConst(DefineParams,'#define','SOURCE_UNIFORM_MPW',[SourceType.UNIFORM_MPW],None)
setConst(DefineParams,'#define','SOURCE_NONE',[SourceType.NONE],None)
setConst(DefineParams,'#define','SOURCE_FORCE_LINE',[SourceType.FORCE_LINE],None)



writeParams("Particles","PartParams.cfg",PartParams)
writeParams("Lasers","LasParams.cfg",LasParams)

writeConst('const.h', 'SysParams.cfg', SysParams)
writeDefine('defines.h',DefineParams)



f = open('phys.par', 'w')
f.write("w_p = " + str(w_p)+"\n")
f.write("1/w_p = " + str(1./w_p)+"\n")
f.write("c/w_p = " + str(cc/w_p)+"\n")
f.close()

f = open('workdir.tmp', 'w')
f.write(WorkDir)
f.close()
f = open('queue.tmp', 'w')
f.write(Queue)
f.close()
f = open('cluster.tmp', 'w')
f.write(Cluster)
f.close()
f = open('proc.tmp', 'w')
f.write(str(NumProcs))
f.close()
